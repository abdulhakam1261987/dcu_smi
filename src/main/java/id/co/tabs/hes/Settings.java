package id.co.tabs.hes;

import java.util.Properties;
import java.io.IOException;
import java.io.InputStream;

public class Settings {
    private static volatile Properties properties;
    private static Boolean loaded = false;

    private Settings() {
    }

    public static Properties getProperties() throws IOException {
        if (!loaded) {
            synchronized (Settings.class) {
                if (!loaded) {
                    properties = new Properties();
                    InputStream inputStream = Settings.class.getClassLoader().getResourceAsStream("app.properties");
                    properties.load(inputStream);
                }
            }
        }
        return properties;
    }
}
