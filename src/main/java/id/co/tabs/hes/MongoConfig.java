package id.co.tabs.hes;

import id.co.tabs.hes.MongoInstance;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MongoConfig {
	static InputStream inputStream;
	    
	public static void mongoProperties() throws IOException {
		try {
			
			Properties prop = new Properties();
			String propFileName = "mongosource.properties";
 
			MongoInstance mg = MongoInstance.getGlobalsInstance(); 
			
			inputStream = MongoConfig.class.getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			
			mg.setUrl(prop.getProperty("mongo.ip"));
			mg.setPort(prop.getProperty("mongo.port"));
			mg.setUser(prop.getProperty("mongo.user"));
			mg.setPassword(prop.getProperty("mongo.password"));
			mg.setDatabase(prop.getProperty("mongo.db"));
 		} catch (Exception e) {
// 			logger.log(Level.WARNING, "Exception " + e);
		} finally {
			inputStream.close();
		}
	}


}
