package id.co.tabs.hes;

import java.io.File;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.List;

public class Utils {
    public static void prepareDirectory(String path) {
        File f = new File(path);
        if (!f.exists())
            f.mkdirs();
    }
    
    /**
     * Salinan dari method showValue() bawaan Gurux di class
     * gurux.dlms.client.GXDLMSReader. Hanya menambahkan return String.
     *
     * @param pos Index attribute.
     * @param value Hasil baca GXDLMSObject.
     *
     * @return Hasil baca dalam bentuk String.
     */
    public static String lihatNilai(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = new String((byte[]) val);
        } else if (val instanceof Double) {
            NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            StringBuilder sb = new StringBuilder();
            for (Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        return String.valueOf(val);
    }
    
    public static String arrayByteToString(byte[] bs) {
        String nilai = "";
        for (byte b : bs) {
            char c = (char) b;
            nilai = nilai + c;
        }
        return nilai;
    }
}
