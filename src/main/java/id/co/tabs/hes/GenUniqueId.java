package id.co.tabs.hes;

import java.util.Date;
import java.util.UUID;

public class GenUniqueId {
	public static String genId(String username){
		UUID myuuid = UUID.randomUUID();
		long lowbits = myuuid.getLeastSignificantBits();
		Date dt = new Date();
		return username+"_"+dt.getTime()+""+lowbits;
	}
}
