package id.co.tabs.hes;

import com.mongodb.DBObject;

import gurux.common.enums.TraceLevel;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.Security;

public class Params {
	InterfaceType interfaceType;
    Boolean useLogicalNameReferencing=false;
    String hostName;
    TraceLevel traceLevel=TraceLevel.OFF;
    Integer port;
    String password;
    Boolean useIEC=false;
    String serialPort;
    String authenticationLevel;
    Integer clientAddress;
    Integer targetAddress;
    // Integer serverAddress;
    Integer physicalAddress;
    Integer logicalAddress;
    String selectedObject;
    Security security;
    String authenticationKey;
    String blockCipherKey;
    String systemTitle;
    Integer phasa;
    String noMeter;
    String typeMeter;
    String merkMeter;
    String kdPusat;
    String kdUnitBisnis;
    String kdPembangkit;
    String kdArea;
    String jenis;
    String noDevice;
    Boolean isDynamic;
    String typeObis;
    String noSeluler;
    String noSimcard;
    DBObject dbObject;
    Integer addressSize;

	/**
	 * @return the addressSize
	 */
	public Integer getAddressSize() {
		return addressSize;
	}

	/**
	 * @param addressSize the addressSize to set
	 */
	public void setAddressSize(Integer addressSize) {
		this.addressSize = addressSize;
	}

	/**
	 * @return the targetAddress
	 */
	public Integer getTargetAddress() {
		return targetAddress;
	}

	/**
	 * @param targetAddress the targetAddress to set
	 */
	public void setTargetAddress(Integer targetAddress) {
		this.targetAddress = targetAddress;
	}

	public InterfaceType getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(String interfaceType) {
        if(interfaceType.equals("WRAPPER")){
            this.interfaceType = InterfaceType.WRAPPER;
        }else if(interfaceType.equals("HDLC")){
            this.interfaceType = InterfaceType.HDLC;
        }else if(interfaceType.equals("PDU")){
            this.interfaceType = InterfaceType.PDU;
        }else if(interfaceType.equals("WIRELESS_MBUS")){
            this.interfaceType = InterfaceType.WIRELESS_MBUS;
        }else{
        	this.interfaceType = InterfaceType.HDLC;
        }
    }

    public Boolean getUseLogicalNameReferencing() {
        return useLogicalNameReferencing;
    }

    public void setUseLogicalNameReferencing(Boolean useLogicalNameReferencing) {
        this.useLogicalNameReferencing = useLogicalNameReferencing;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public TraceLevel getTraceLevel() {
        return traceLevel;
    }

    public void setTraceLevel(String traceLevel) {
        if(traceLevel.equals("ERROR"))
            this.traceLevel = TraceLevel.ERROR;
        else if(traceLevel.equals("WARNING"))
            this.traceLevel = TraceLevel.WARNING;
        else if(traceLevel.equals("INFO"))
            this.traceLevel = TraceLevel.INFO;
        else if(traceLevel.equals("OFF"))
            this.traceLevel = TraceLevel.OFF;
        else if(traceLevel.equals("VERBOSE"))
            this.traceLevel = TraceLevel.VERBOSE;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getUseIEC() {
        return useIEC;
    }

    public void setUseIEC(Boolean useIEC) {
        this.useIEC = useIEC;
    }

    public String getSerialPort() {
        return serialPort;
    }

    public void setSerialPort(String serialPort) {
        this.serialPort = serialPort;
    }

    public String getAuthenticationLevel() {
        return authenticationLevel;
    }

    public void setAuthenticationLevel(String authenticationLevel) {
        this.authenticationLevel = authenticationLevel;
    }

    public Integer getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(Integer clientAddress) {
        this.clientAddress = clientAddress;
    }

    // public Integer getServerAddress() {
    //     return serverAddress;
    // }

    // public void setServerAddress(Integer serverAddress) {
    //     this.serverAddress = serverAddress;
    // }

    public String getSelectedObject() {
        return selectedObject;
    }

    public void setSelectedObject(String selectedObject) {
        this.selectedObject = selectedObject;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        if(security.equals("AUTHENTICATION_ENCRYPTION")){
            this.security = Security.AUTHENTICATION_ENCRYPTION;
        }else if(security.equals("AUTHENTICATION")){
            this.security = Security.AUTHENTICATION;
        }else if(security.equals("ENCRYPTION")){
            this.security = Security.ENCRYPTION;
        }else if(security.equals("NONE")){
            this.security = Security.NONE;
        }
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getBlockCipherKey() {
        return blockCipherKey;
    }

    public void setBlockCipherKey(String blockCipherKey) {
        this.blockCipherKey = blockCipherKey;
    }

    public String getSystemTitle() {
        return systemTitle;
    }

    public void setSystemTitle(String systemTitle) {
        this.systemTitle = systemTitle;
    }

    public Integer getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(Integer physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public Integer getLogicalAddress() {
        return logicalAddress;
    }

    public void setLogicalAddress(Integer logicalAddress) {
        this.logicalAddress = logicalAddress;
    }

	/**
	 * @return the phasa
	 */
	public Integer getPhasa() {
		return phasa;
	}

	/**
	 * @param phasa the phasa to set
	 */
	public void setPhasa(Integer phasa) {
		this.phasa = phasa;
	}

	/**
	 * @return the noMeter
	 */
	public String getNoMeter() {
		return noMeter;
	}

	/**
	 * @param noMeter the noMeter to set
	 */
	public void setNoMeter(String noMeter) {
		this.noMeter = noMeter;
	}

	/**
	 * @return the kdPusat
	 */
	public String getKdPusat() {
		return kdPusat;
	}

	/**
	 * @param kdPusat the kdPusat to set
	 */
	public void setKdPusat(String kdPusat) {
		this.kdPusat = kdPusat;
	}

	/**
	 * @return the kdUnitBisnis
	 */
	public String getKdUnitBisnis() {
		return kdUnitBisnis;
	}

	/**
	 * @param kdUnitBisnis the kdUnitBisnis to set
	 */
	public void setKdUnitBisnis(String kdUnitBisnis) {
		this.kdUnitBisnis = kdUnitBisnis;
	}

	/**
	 * @return the kdPembangkit
	 */
	public String getKdPembangkit() {
		return kdPembangkit;
	}

	/**
	 * @param kdPembangkit the kdPembangkit to set
	 */
	public void setKdPembangkit(String kdPembangkit) {
		this.kdPembangkit = kdPembangkit;
	}

	/**
	 * @return the kdArea
	 */
	public String getKdArea() {
		return kdArea;
	}

	/**
	 * @param kdArea the kdArea to set
	 */
	public void setKdArea(String kdArea) {
		this.kdArea = kdArea;
	}

	/**
	 * @return the jenis
	 */
	public String getJenis() {
		return jenis;
	}

	/**
	 * @param jenis the jenis to set
	 */
	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	/**
	 * @return the typeMeter
	 */
	public String getTypeMeter() {
		return typeMeter;
	}

	/**
	 * @param typeMeter the typeMeter to set
	 */
	public void setTypeMeter(String typeMeter) {
		this.typeMeter = typeMeter;
	}

	/**
	 * @return the merkMeter
	 */
	public String getMerkMeter() {
		return merkMeter;
	}

	/**
	 * @param merkMeter the merkMeter to set
	 */
	public void setMerkMeter(String merkMeter) {
		this.merkMeter = merkMeter;
	}

	/**
	 * @return the noModem
	 */
	public String getNoDevice() {
		return noDevice;
	}

	/**
	 * @param noModem the noModem to set
	 */
	public void setNoDevice(String noDevice) {
		this.noDevice = noDevice;
	}

	/**
	 * @return the isDynamic
	 */
	public Boolean getIsDynamic() {
		return isDynamic;
	}

	/**
	 * @param isDynamic the isDynamic to set
	 */
	public void setTypeDevice(String typeDevice) {
		if(typeDevice!=null && typeDevice.equals("DYNAMIC")){
			this.isDynamic = true;
		}else{
			this.isDynamic = false;
		}
	}

	/**
	 * @return the isSPLN
	 */
	public String gettypeObis() {
		return typeObis;
	}

	/**
	 * @param isSPLN the isSPLN to set
	 */
	public void setTypeObis(String typeObis) {
		if(typeObis==null ||typeObis.equals("")){
			this.typeObis = "SPLN";
		}else{
			this.typeObis = typeObis;
		}
	}

	/**
	 * @return the noSeluler
	 */
	public String getNoSeluler() {
		return noSeluler;
	}

	/**
	 * @param noSeluler the noSeluler to set
	 */
	public void setNoSeluler(String noSeluler) {
		this.noSeluler = noSeluler;
	}

	/**
	 * @return the noSimcard
	 */
	public String getNoSimcard() {
		return noSimcard;
	}

	/**
	 * @param noSimcard the noSimcard to set
	 */
	public void setNoSimcard(String noSimcard) {
		this.noSimcard = noSimcard;
	}

	/**
	 * @return the dataMongo
	 */
	public DBObject getDbObject() {
		return dbObject;
	}

	/**
	 * @param dataMongo the dataMongo to set
	 */
	public void setDbObject(DBObject dbObject) {
		if(dbObject!=null){
			dbObject.removeField("AUTHENTICATION_LEVEL");
			dbObject.removeField("CLIENT_ADDRESS");
			dbObject.removeField("LOGICAL_ADDRESS");
			dbObject.removeField("PHYSICAL_ADDRESS");
			dbObject.removeField("PASSWORD_METER");
			dbObject.removeField("IP_ADDRESS");
			dbObject.removeField("PORT");
			dbObject.removeField("SECURITY");
			dbObject.removeField("AUTHENTICATION_KEY");
			dbObject.removeField("BLOCK_CIPHER_KEY");
			dbObject.removeField("SYSTEM_TITLE");
		}
		this.dbObject = dbObject;
	}



}
