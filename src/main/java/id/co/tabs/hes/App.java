package id.co.tabs.hes;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import id.co.tabs.hes.api.HelloReply;
import id.co.tabs.hes.api.HelloRequest;
import id.co.tabs.hes.api.SetDataMultiLNRequest;
import id.co.tabs.hes.api.SetDataRequest;
import id.co.tabs.hes.api.ActionRequest;
import id.co.tabs.hes.api.GetDataMultiLNRequest;
import id.co.tabs.hes.api.GetDataProfileRequest;
import id.co.tabs.hes.api.GetDataRequest;
import id.co.tabs.hes.api.GetDataResponse;
import id.co.tabs.hes.api.GetOneDataRequest;
import id.co.tabs.hes.api.GreeterGrpc;
import id.co.tabs.hes.dcu.DCU;
import id.co.tabs.hes.dcu.DCUManager;
import id.co.tabs.hes.dcu.IDCUManagerListener;
import id.co.tabs.hes.dcu.Listener;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

public final class App {

    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    private App() {
    }

    /**
     * Says hello to the world.
     *
     * @param args The arguments of the program.
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // int a = Integer.parseInt("2036710") ;
        // System.out.println(a);
        // DCU dcu = new DCU(null,"","");
        // final HashMap<String, String> MapAuthMeter =  dcu.MappingAuthMeter("eyJnZXRSZXN1bHQiOiIxIiwgImF1dGhlbnRpY2F0aW9uTGV2ZWwiOiJIaWdoR01hYyIsImNsaWVudEFkZHJlc3MiOiIxIiwibG9naWNhbEFkZHJlc3MiOiIyMDM2NzEwOCIsInBoeXNpY2FsQWRkcmVzcyI6IjgxOTE5IiwicGFzc01ldGVyIjoiMDAwMDAwMDAiLCJzZWN1cml0eSI6IkFVVEhFTlRJQ0FUSU9OX0VOQ1JZUFRJT04iLCJhdXRoZW50aWNhdGlvbktleSI6IjAwMDAwMDAwMDAwMDAwMDAiLCJibG9ja0NpcGhlcktleSI6IjAwMDAwMDAwMDAwMDAwMDAiLCJzeXN0ZW1UaXRsZSI6IkhYRSAwICAgIiwicGhhc2EiOiIiLCJ0eXBlTWV0ZXIiOiIiLCJpbnRlcmZhY2VUeXBlIjoiV1JBUFBFUiIsInRhcmdldEFkZHJlc3MiOiIiLCJhZGRyZXNzU2l6ZSI6IjIiLCJwb3J0IjoiIn0=");
        MongoConfig.mongoProperties();
        // Initialize directory structure
        Utils.prepareDirectory("log");
        Utils.prepareDirectory("db");
        Utils.prepareDirectory("cache");

        // Initialize logging
        InputStream stream = App.class.getClassLoader().getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOGGER.info("Head End System Version 1.0");
        LOGGER.info("Copyright © 2019 by PT. TAB Solutions. All rights reserved.");
        LOGGER.info("");

        LOGGER.info("Initializing database subsystem...");
        DB.initialize();

        DCUManager.getInstance().addListener(new IDCUManagerListener() {

            @Override
            public void onDCUConnected(DCU dcu) {
                // TODO Auto-generated method stub
                // .. dcu.testRead();
            }

            @Override
            public void onDCUDisconnected(DCU dcu) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onDCUHeartbeat(final DCU dcu) {
                // TODO Auto-generated method stub
                Thread t = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        dcu.testRead();

                    }

                });
                t.start();

            }

        });
        Integer listenerPort = Integer.valueOf(Settings.getProperties().getProperty("listener.port", "6002"));
        LOGGER.info("Opening DCU listener on port " + listenerPort.toString() + "...");
        final Listener listener = new Listener(listenerPort);
        LOGGER.info("System is up and running.");

        /* The port on which the server should run */
        int port = 50062;
        Server server = ServerBuilder.forPort(port).addService(new GreeterImpl()).build().start();
        LOGGER.info("Server started, listening on " + port);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                LOGGER.info("System is shutting down...");
                listener.close();
            }
        });
    }

    static class GreeterImpl extends GreeterGrpc.GreeterImplBase {

        @Override
        public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN("07310001");
            dcu.testRead();
            HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName()).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void readClock(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            List<String> json = dcu.ReadClock(req.getNoMeter(), req.getAuthMeter(), req.getAuthDCU());
            HelloReply reply = HelloReply.newBuilder().setMessage(json.get(0)).setResult(json.get(1))
                    .setOut(json.get(2)).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void writeClock(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            List<String> json = dcu.writeClock(req.getNoMeter(), req.getValue(), req.getAuthMeter(), req.getAuthDCU());
            HelloReply reply = HelloReply.newBuilder().setMessage(json.get(0)).setResult(json.get(1))
                    .setOut(json.get(2)).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void disconnectReconnect(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            List<String> json = dcu.disconnectReconnect(req.getNoMeter(), Integer.parseInt(req.getValue()), req.getAuthMeter(), req.getAuthDCU());
            HelloReply reply = HelloReply.newBuilder().setMessage(json.get(0)).setResult(json.get(1))
                    .setOut(json.get(2)).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void getListMeter(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            List<String> json = dcu.ReadListMeter(req.getAuthDCU());
            HelloReply reply = HelloReply.newBuilder().setMessage(json.get(0)).setResult(json.get(1))
                    .setOut(json.get(2)).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void readLoadProfile(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            List<String> json = dcu.ReadLoadProfile(req.getNoMeter(), req.getValue(), req.getValue2(), req.getAuthMeter(), req.getAuthDCU());
            HelloReply reply = HelloReply.newBuilder().setMessage(json.get(0)).setResult(json.get(1))
                    .setOut(json.get(2)).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        //New Function...
        @Override
        public void getOneData(GetOneDataRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            System.out.println("No DCU : " + req.getNoDCU());
            System.out.println("No Meter : " + req.getNoMeter());
            System.out.println("Logical Name : " + req.getLogicalName());
            System.out.println("Class Id : " + req.getClassId());
            System.out.println("Nama Field : " + req.getNamaField());
            GetDataResponse reply = dcu.readOneData(req.getNoMeter(), req.getLogicalName(), req.getClassId(),
                    req.getNamaField(), UID, p);
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void getData(GetDataRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());

            Params p = setParam(req.getNoMeter(), null);
            System.out.println("...... Mulai");
            GetDataResponse reply = GetDataResponse.newBuilder().build();
            try {
                System.out.println(req.getNoMeter());
                System.out.println(req.getJenis());
                System.out.println(UID);
                // System.out.println(p.getJenis());
                final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
                reply = dcu.readData(req.getNoMeter(), req.getJenis(), UID, p);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("...... Selesai");
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void setData(SetDataRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            GetDataResponse reply = dcu.writeData(req.getNoMeter(), req.getJenis(), UID, p, req.getValue());
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void getDataMultiLN(GetDataMultiLNRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            String lm = req.getLogicalName();
            JSONParser parser = new JSONParser();
            List<DataLogicalName> lln = new ArrayList<DataLogicalName>();
            try {
                JSONArray jArray = (JSONArray) parser.parse(lm);
                for (int i = 0; i < jArray.size(); i++) {
                    JSONObject json = (JSONObject) jArray.get(i);
                    Gson g = new Gson();
                    DataLogicalName object = g.fromJson(json.toString(), DataLogicalName.class);
                    lln.add(object);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ListLogicalName lst = new ListLogicalName();
            if (lln.size() > 0) {
                lst.setListObis(lln);
            }
            GetDataResponse reply = dcu.getMultiLogicalName(lst, UID, req.getNoMeter(), p);
//			GetDataResponse reply = GetDataResponse.newBuilder().setGreeting(jRes.toJSONString()).build();
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void setDataMultiLN(SetDataMultiLNRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            String lm = req.getLogicalName();
            JSONParser parser = new JSONParser();
            List<DataLogicalName> lln = new ArrayList<DataLogicalName>();
            try {
                JSONArray jArray = (JSONArray) parser.parse(lm);
                for (int i = 0; i < jArray.size(); i++) {
                    JSONObject json = (JSONObject) jArray.get(i);
                    Gson g = new Gson();
                    DataLogicalName object = g.fromJson(json.toString(), DataLogicalName.class);
                    lln.add(object);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ListLogicalName lst = new ListLogicalName();
            if (lln.size() > 0) {
                lst.setListObis(lln);
            }

            GetDataResponse reply = dcu.setMultiLogicalName(lst, UID, req.getNoMeter(), p);
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }

        @Override
        public void getDataProfile(GetDataProfileRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            try {
                java.util.Calendar startLP = java.util.Calendar.getInstance();
                startLP.setTime(sdf.parse(req.getTanggalAwal()));

                java.util.Calendar endLp = java.util.Calendar.getInstance();
                endLp.setTime(sdf.parse(req.getTanggalAkhir()));

                GetDataResponse reply = dcu.readDataProfile(req.getNoMeter(), req.getJenis(),
                        startLP, endLp, UID, p);
                responseObserver.onNext(reply);
            } catch (Exception e) {
                e.printStackTrace();
            }
            responseObserver.onCompleted();
        }

        @Override
        public void action(ActionRequest req, StreamObserver<GetDataResponse> responseObserver) {
            String UID = GenUniqueId.genId(req.getUserId());
            final DCU dcu = DCUManager.getInstance().getDCUBySN(req.getNoDCU());
            Params p = setParam(req.getNoMeter(), null);
            GetDataResponse reply = dcu.actionData(req.getNoMeter(), req.getJenis(), UID, p, req.getValue());
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }

    static Params setParam(String nometer, HashMap<String, String> authMeter) {

        Params p = new Params();
        try {
            BasicDBObject query = new BasicDBObject();
            query.put("NO_METER", nometer);
            MongoDB db = new MongoDB();
            List<DBObject> cursor = db.getData("DATA_METER", query, "");
            String noMeter = nometer;
            String typeMeter = "";
            String merkMeter = "";
            String authenticationLevel = "";
            String clientAddress = "";
            String logicalAddress = "";
            String physicalAddress = "";
            String ipAddress = "";
            String port = "";
            String passMeter = "";
            String security = "";
            String authenticationKey = "";
            String blockCipherKey = "";
            String systemTitle = "";
            Integer phasa = 0;
            String kdPusat = "";
            String kdPembangkit = "";
            String kdArea = "";
            String kdUnitBisnis = "";
            String jenis = "";

            if (cursor.size() > 0) {
                for (DBObject csr : cursor) {
                    noMeter = csr.get("NO_METER").toString();
                    typeMeter = csr.get("TYPE_METER").toString();
                    merkMeter = csr.get("MERK_METER").toString();
                    authenticationLevel = csr.get("AUTHENTICATION_LEVEL").toString();
                    clientAddress = csr.get("CLIENT_ADDRESS").toString();
//					logicalAddress = csr.get("LOGICAL_ADDRESS").toString();
                    logicalAddress = "0";
                    physicalAddress = csr.get("PHYSICAL_ADDRESS").toString();
                    ipAddress = csr.get("IP_ADDRESS").toString();
                    port = csr.get("PORT").toString();
                    passMeter = csr.get("PASSWORD_METER").toString();
                    security = csr.get("SECURITY").toString();
                    authenticationKey = csr.get("AUTHENTICATION_KEY").toString();
                    blockCipherKey = csr.get("BLOCK_CIPHER_KEY").toString();
                    systemTitle = csr.get("SYSTEM_TITLE").toString();
                    phasa = Integer.parseInt(csr.get("PHASA").toString());
                    kdPusat = csr.get("KD_PUSAT").toString();
                    kdPembangkit = csr.get("KD_PEMBANGKIT").toString();
                    kdArea = csr.get("KD_AREA").toString();
                    kdUnitBisnis = csr.get("KD_UNIT_BISNIS").toString();
                    jenis = csr.get("JENIS").toString();
                }
            }

            if (authMeter != null && authMeter.size() > 0) {
                if (authMeter.get("authenticationLevel") != null && !authMeter.get("authenticationLevel").equals("")) {
                    authenticationLevel = authMeter.get("authenticationLevel");
                }

                if (authMeter.get("clientAddress") != null && !authMeter.get("clientAddress").equals("")) {
                    clientAddress = authMeter.get("clientAddress");
                }

                if (authMeter.get("logicalAddress") != null && !authMeter.get("logicalAddress").equals("")) {
                    logicalAddress = authMeter.get("logicalAddress");
                }

                if (authMeter.get("physicalAddress") != null && !authMeter.get("physicalAddress").equals("")) {
                    physicalAddress = authMeter.get("physicalAddress");
                }

                if (authMeter.get("ipAddress") != null && !authMeter.get("ipAddress").equals("")) {
                    ipAddress = authMeter.get("ipAddress");
                }

                if (authMeter.get("port") != null && !authMeter.get("port").equals("")) {
                    port = authMeter.get("port");
                }

                if (authMeter.get("passMeter") != null && !authMeter.get("passMeter").equals("")) {
                    passMeter = authMeter.get("passMeter");
                }

                if (authMeter.get("security") != null && !authMeter.get("security").equals("")) {
                    security = authMeter.get("security");
                }

                if (authMeter.get("authenticationKey") != null && !authMeter.get("authenticationKey").equals("")) {
                    authenticationKey = authMeter.get("authenticationKey");
                }

                if (authMeter.get("blockCipherKey") != null && !authMeter.get("blockCipherKey").equals("")) {
                    blockCipherKey = authMeter.get("blockCipherKey");
                }

                if (authMeter.get("systemTitle") != null && !authMeter.get("systemTitle").equals("")) {
                    systemTitle = authMeter.get("systemTitle");
                }

                if (authMeter.get("phasa") != null && !authMeter.get("phasa").equals("")) {
                    phasa = Integer.parseInt(authMeter.get("phasa"));
                }

                if (authMeter.get("merkMeter") != null && !authMeter.get("merkMeter").equals("")) {
                    merkMeter = authMeter.get("merkMeter");
                }

                if (authMeter.get("typeMeter") != null && !authMeter.get("typeMeter").equals("")) {
                    typeMeter = authMeter.get("typeMeter");
                }
            }

            p.setAuthenticationLevel(authenticationLevel);
            p.setUseLogicalNameReferencing(true);
            p.setClientAddress(Integer.parseInt(clientAddress));
            p.setLogicalAddress(Integer.parseInt(logicalAddress));
            p.setPhysicalAddress(Integer.parseInt(physicalAddress));
            p.setPassword(passMeter);
            p.setTraceLevel("OFF");
            p.setHostName(ipAddress);
            p.setPort(Integer.parseInt(port));
            p.setSecurity(security);
            p.setAuthenticationKey(authenticationKey);
            p.setBlockCipherKey(blockCipherKey);
            p.setSystemTitle(systemTitle);
            p.setPhasa(phasa);
            p.setNoMeter(noMeter);
            p.setTypeMeter(typeMeter);
            p.setMerkMeter(merkMeter);
            p.setKdArea(kdArea);
            p.setKdPembangkit(kdPembangkit);
            p.setKdPusat(kdPusat);
            p.setKdUnitBisnis(kdUnitBisnis);
            p.setJenis(jenis);
            // int ret = getParameters(settings, p);
            // if (ret != 0) {
            // 	return null;
            // }
        } catch (Exception e) {
            e.printStackTrace();
            return p;
        }
        return p;
    }
}
