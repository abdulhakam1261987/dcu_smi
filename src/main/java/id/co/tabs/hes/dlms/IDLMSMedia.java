package id.co.tabs.hes.dlms;

public interface IDLMSMedia {
    void send(final Object data);
    boolean receive();
    byte[] getReceivedData();
}
