/*
 * 
 */
package id.co.tabs.hes.dlms.object;

import id.co.tabs.hes.Utils;
import java.util.List;
import java.util.logging.Logger;

/**
 * Meter dalam list DCU
 *
 * @author Tab Solutions
 */
public class Meter {

    private long enumerationNumber;
    private String logicalDeviceName;
    private int portNo;
    private int baudRate;
    private int commType;
    private int dataBitLength;
    private int dataTransmissionStartBit;
    private int dataTransmissionStopBit;
    private int dataTransmissionCheckBit;
    private int dataTransparentMeterTariff;
    private int wiringMethod;
    private static final Logger LOG = Logger.getLogger(Meter.class.getName());

    public Meter() {
        this.baudRate = 5;
        this.commType = 3;
        this.wiringMethod = 0;
    }

    public Meter(long enumerationNumber, String logicalDeviceName, int portNo, int baudRate, int commType, int dataBitLength, int dataTransmissionStartBit, int dataTransmissionStopBit, int dataTransmissionCheckBit, int dataTransparentMeterTariff, int wiringMethod) {
        this.enumerationNumber = enumerationNumber;
        this.logicalDeviceName = logicalDeviceName;
        this.portNo = portNo;
        this.baudRate = baudRate;
        this.commType = commType;
        this.dataBitLength = dataBitLength;
        this.dataTransmissionStartBit = dataTransmissionStartBit;
        this.dataTransmissionStopBit = dataTransmissionStopBit;
        this.dataTransmissionCheckBit = dataTransmissionCheckBit;
        this.dataTransparentMeterTariff = dataTransparentMeterTariff;
        this.wiringMethod = wiringMethod;
    }

    public long getEnumerationNumber() {
        return enumerationNumber;
    }

    public void setEnumerationNumber(long enumerationNumber) {
        this.enumerationNumber = enumerationNumber;
    }

    public String getLogicalDeviceName() {
        return logicalDeviceName;
    }

    public void setLogicalDeviceName(String logicalDeviceName) {
        this.logicalDeviceName = logicalDeviceName;
    }

    public int getPortNo() {
        return portNo;
    }

    public void setPortNo(int portNo) {
        this.portNo = portNo;
    }

    public int getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(int baudRate) {
        this.baudRate = baudRate;
    }

    public int getCommType() {
        return commType;
    }

    public void setCommType(int commType) {
        this.commType = commType;
    }

    public int getDataBitLength() {
        return dataBitLength;
    }

    public void setDataBitLength(int dataBitLength) {
        this.dataBitLength = dataBitLength;
    }

    public int getDataTransmissionStartBit() {
        return dataTransmissionStartBit;
    }

    public void setDataTransmissionStartBit(int dataTransmissionStartBit) {
        this.dataTransmissionStartBit = dataTransmissionStartBit;
    }

    public int getDataTransmissionStopBit() {
        return dataTransmissionStopBit;
    }

    public void setDataTransmissionStopBit(int dataTransmissionStopBit) {
        this.dataTransmissionStopBit = dataTransmissionStopBit;
    }

    public int getDataTransmissionCheckBit() {
        return dataTransmissionCheckBit;
    }

    public void setDataTransmissionCheckBit(int dataTransmissionCheckBit) {
        this.dataTransmissionCheckBit = dataTransmissionCheckBit;
    }

    public int getDataTransparentMeterTariff() {
        return dataTransparentMeterTariff;
    }

    public void setDataTransparentMeterTariff(int dataTransparentMeterTariff) {
        this.dataTransparentMeterTariff = dataTransparentMeterTariff;
    }

    public int getWiringMethod() {
        return wiringMethod;
    }

    public void setWiringMethod(int wiringMethod) {
        this.wiringMethod = wiringMethod;
    }

    @Override
    public String toString() {
        String result = "";
        result += "Meter " + logicalDeviceName + "{" + ""
                + "\n\tEnumeration number = " + enumerationNumber
                + ",\n\tLogical device name = " + logicalDeviceName
                + ",\n\tPort no = " + portNo;
        switch (portNo) {
            case 1:
                result += " (485-1)";
                break;
            case 2:
                result += " (485-2)";
                break;
            case 30:
                result += " (RF Channel)";
                break;
            case 31:
                result += " (PLC Channel)";
                break;
            default:
                result += " (Unknown)";
                break;
        }
        result += ",\n\tBaud rate = " + baudRate;
        switch (baudRate) {
            case 0:
                result += " (300 bps)";
                break;
            case 1:
                result += " (600 bps)";
                break;
            case 2:
                result += " (1200 bps)";
                break;
            case 3:
                result += " (2400 bps)";
                break;
            case 4:
                result += " (4800 bps)";
                break;
            case 5:
                result += " (9600 bps)";
                break;
            case 6:
                result += " (19200 bps)";
                break;
            case 7:
                result += " (38400 bps)";
                break;
            case 8:
                result += " (57600 bps)";
                break;
            case 9:
                result += " (115200 bps)";
                break;
            default:
                result += " (Unknown)";
                break;
        }
        result += ",\n\tCommunication protocol type = " + commType;
        result += commType == 3 ? " (DLMS)" : " (Unknown)";
        result += ",\n\tData bit length = " + dataBitLength
                + ",\n\tData transmission start bit = " + dataTransmissionStartBit
                + ",\n\tData transmission stop bit = " + dataTransmissionStopBit
                + ",\n\tData transmission check bit = " + dataTransmissionCheckBit
                + ",\n\tData transparent meter tariff = " + dataTransparentMeterTariff
                + ",\n\tWiring method = " + wiringMethod;
        switch (wiringMethod) {
            case 0:
                result += " (Single phase)";
                break;
            case 1:
                result += " (Three-phase three-wire)";
                break;
            case 2:
                result += " (Three-phase four-wire)";
                break;
            default:
                result += " (Unknown)";
                break;
        }
        return result += "}";
    }

    public void setBykodeMeterParameterForInt(int kode, int nilai) {
        switch (kode) {
            case 1:
                this.setEnumerationNumber(nilai);
                break;
            case 3:
                this.setPortNo(nilai);
                break;
            case 4:
                this.setBaudRate(nilai);
                break;
            case 5:
                this.setCommType(nilai);
                break;
            case 6:
                this.setDataBitLength(nilai);
                break;
            case 7:
                this.setDataTransmissionStartBit(nilai);
                break;
            case 8:
                this.setDataTransmissionStopBit(nilai);
                break;
            case 9:
                this.setDataTransmissionCheckBit(nilai);
                break;
            case 10:
                this.setDataTransparentMeterTariff(nilai);
                break;
            case 11:
                this.setWiringMethod(nilai);
                break;
        }
    }

    public static Meter parseMeterFromObject(List tmp) {
        Meter meter = new Meter();
        int kode = 1;
        for (Object object : (List) tmp) {
            if (object instanceof Integer) {
                int nilai = (int) object;
                meter.setBykodeMeterParameterForInt(kode, nilai);
                kode++;
            } else if (object instanceof byte[]) {
                byte[] bs = (byte[]) object;
                meter.setLogicalDeviceName(Utils.arrayByteToString(bs));
                kode++;
            } else {
                meter.setBykodeMeterParameterForInt(kode, Integer.parseInt(String.valueOf(object)));
                kode++;
            }
        }
        return meter;
    }
}
