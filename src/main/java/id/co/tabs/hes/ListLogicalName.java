package id.co.tabs.hes;

import java.util.List;
public class ListLogicalName {
	List<DataLogicalName> listObis;

	/**
	 * @return the listObis
	 */
	public List<DataLogicalName> getListObis() {
		return listObis;
	}

	/**
	 * @param listObis the listObis to set
	 */
	public void setListObis(List<DataLogicalName> listObis) {
		this.listObis = listObis;
	}

	
}
