package id.co.tabs.hes;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoInstance {
private static MongoInstance globalsInstance = new MongoInstance();
	
	public static MongoInstance getInstance() {
        return globalsInstance;
    }
	
	private String url;
	private String port;
	private String user;
	private String password;
	private String database;
	private MongoClient mongoClient;
	private DB db;

	/**
	 * @return the globalsInstance
	 */
	public static MongoInstance getGlobalsInstance() {
		return globalsInstance;
	}
	/**
	 * @param globalsInstance the globalsInstance to set
	 */
	public static void setGlobalsInstance(MongoInstance globalsInstance) {
		MongoInstance.globalsInstance = globalsInstance;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}
	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the mongoClient
	 */
	public MongoClient getMongoClient() {
		return mongoClient;
	}
	/**
	 * @param mongoClient the mongoClient to set
	 */
	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}
	/**
	 * @return the db
	 */
	public DB getDb() {
		return db;
	}
	/**
	 * @param db the db to set
	 */
	public void setDb(DB db) {
		this.db = db;
	}
	
	
}
