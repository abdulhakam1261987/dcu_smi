package id.co.tabs.hes;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import id.co.tabs.hes.MongoInstance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;
import org.bson.conversions.Bson;

public class MongoDB {

    private static final Logger LOGGER = Logger.getLogger(MongoDB.class.getName());
    private static MongoClient mongoClient;
    private static MongoInstance mg = MongoInstance.getGlobalsInstance();
    private static String Mongodb = mg.getDatabase();
    private static String usr = mg.getUser();
    private static String pwd = mg.getPassword();
    private static String addrs = mg.getUrl();
    private static Integer port = Integer.parseInt(mg.getPort());

    private static MongoClient getConnection() {
        if(mongoClient==null){
            MongoCredential credential = MongoCredential.createCredential(usr, Mongodb, pwd.toCharArray());
            mongoClient = new MongoClient(new ServerAddress(addrs, port), Arrays.asList(credential));
        }
        return mongoClient;
    }

    private static void log(final Level level, final String message) {
        LOGGER.log(level, message);
    }

    public static HashMap<String, String> getDataObis(String obisParam) {

        MongoClient mongoClient = getConnection();
        DB db = mongoClient.getDB(Mongodb);
        DBCollection collection = db.getCollection("DATA_OBIS");
        DBCursor cursor = collection.find(new BasicDBObject(obisParam, new BasicDBObject("$ne", 0)));
        HashMap<String, String> list = new HashMap<String, String>();
        if (cursor.size() < 1) {
            return null;
        }
        for (DBObject result : cursor) {
            list.put(result.get("LOGICAL_NAME").toString(), result.get("NAMA_FIELD").toString());
        }
        return list;
    }

    public List<DBObject> getData(String collectionName, BasicDBObject query, String sort) throws Exception {
        System.out.println("------------------------");
        log(Level.INFO, "getData..." + collectionName);
        MongoClient mongoClient = getConnection();
        DB db = mongoClient.getDB(Mongodb);
        DBCollection collection = db.getCollection(collectionName);
        DBCursor cursor = null;
        if (sort == null || sort == "") {
            cursor = collection.find(query).sort(new BasicDBObject("_id", 1));
        } else {
            cursor = collection.find(query).sort(new BasicDBObject(sort, 1));
        }
        log(Level.INFO, "DBCursor..." + collectionName);
        List<DBObject> fields = new ArrayList<DBObject>();
        while (cursor.hasNext()) {
            DBObject csr = cursor.next();
            fields.add(csr);
        }
        // mongoClient.close();
        return fields;
    }

    public static boolean setData(String collectionName, Document doc) throws Exception {
        MongoClient mongoClient = getConnection();

        // connect to database mongo
        MongoDatabase database = mongoClient.getDatabase(Mongodb);

        // connect table instant
        MongoCollection<Document> collections = database.getCollection(collectionName);
        // insert ke collection (table) instant
        collections.insertOne(doc);
        // mongoClient.close();
        return true;
    }

    public static boolean setMultiData(String collectionName, List<Document> docs) throws Exception {
        System.out.println("setMultiData " + collectionName + " : " + docs.size() + " ");
        MongoClient mongoClient = getConnection();

        // connect to database mongo
        MongoDatabase database = mongoClient.getDatabase(Mongodb);

        // connect table instant
        MongoCollection<Document> collections = database.getCollection(collectionName);
        // insert ke collection (table) instant
        collections.insertMany(docs);
        // mongoClient.close();
        return true;
    }

    public static boolean updateOne(String collectionName, Bson filter, Bson query) throws Exception {
        MongoClient mongoClient = getConnection();

        // connect to database mongo
        MongoDatabase database = mongoClient.getDatabase(Mongodb);

        // connect table instant
        MongoCollection<Document> collections = database.getCollection(collectionName);
        // update one ke collection (table) instant
        collections.updateOne(filter, query);
        // mongoClient.close();
        return true;
    }

    public static List<DBObject> getData(String collectionName, BasicDBObject query, String sort, DBObject projection)
            throws Exception {
        MongoClient mongoClient = getConnection();
        DB db = mongoClient.getDB(Mongodb);
        DBCollection collection = db.getCollection(collectionName);
        DBObject Projection = new BasicDBObject("_id", 0);
        if (projection != null) {
            Projection = projection;
        }
        DBCursor cursor = null;
        if (sort == null || sort == "") {
            cursor = collection.find(query, Projection).sort(new BasicDBObject("_id", 1));
        } else {
            cursor = collection.find(query, Projection).sort(new BasicDBObject(sort, 1));
        }

        List<DBObject> fields = new ArrayList<DBObject>();
        while (cursor.hasNext()) {
            DBObject csr = cursor.next();
            fields.add(csr);
        }
        // mongoClient.close();
        return fields;
    }
}
