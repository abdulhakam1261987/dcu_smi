package id.co.tabs.hes;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DB {
    private static final Logger LOGGER = Logger.getLogger(DB.class.getName());

    private static SessionFactory sessionFactory = null;

    public static void initialize() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration().buildSessionFactory();
        }
    }

    public static Session getSession() throws HibernateException {
        Session session = null;
        try {
            session = sessionFactory.openSession();
        } catch (Throwable t) {
            LOGGER.log(Level.SEVERE, "Exception on Hibernate session", t);
        }
        if (session == null) {
            LOGGER.log(Level.SEVERE, "Hibernate session is null!");
        }

        return session;
    }
}
