package id.co.tabs.hes.dcu.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.bson.Document;

import gurux.dlms.enums.DataType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.enums.ControlMode;
import gurux.dlms.objects.enums.ControlState;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.ResultRead;
import id.co.tabs.hes.dlms.DLMS;

public class DLMSReader {
    private static final Logger LOGGER = Logger.getLogger(DLMSReader.class.getName());
    private String getLogPrefix(final String serialNumber, final String currentMeter) {
        if (currentMeter == null)
            return "DCU@" + serialNumber;
        else
            return "DCU@" + serialNumber + " Meter@" + currentMeter;
    }
    private static void log(final Level level, final String message) {
        // LOGGER.log(level, getLogPrefix(serialNumber, currentMeter) + " " + message);
        LOGGER.log(level, message);
    }
    public static void _DLMSreadOneData(final DLMS r, final String logicalName, final String classId, final String namaField,
            final String UID, final Params p) {
        try {
            java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
            final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID", UID)
                    .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER", p.getTypeMeter())
                    .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
                    .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT", p.getKdPembangkit())
                    .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());

            final String obis = logicalName;
            final String type = classId;
            final String column = namaField;
            if (classId.equals("70")) {
                _readDisconnectControl(r, logicalName, classId, namaField, doc, UID, p);
            } else if (!classId.equals("8")) {
                final GXObisCode skipItem = new GXObisCode();
                skipItem.setObjectType(r.getObjectType(type));
                skipItem.setLogicalName(obis);
                skipItem.setVersion(0);
                skipItem.setDescription(obis);
                List<ResultRead> ldt = new ArrayList<>();
                ldt = r.getRegister(skipItem);
                for (int j = 0; j < ldt.size(); j++) {
                    doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
                    doc.append("DATA", r.convertData(ldt.get(j)));
                }
                now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                doc.append("TGL_SELESAI", now.getTime());
                MongoDB.setData("DATA_GLOBAL", doc);
            } else if (!classId.equals("7")) {
                final GXObisCode skipItem = new GXObisCode();
                skipItem.setObjectType(r.getObjectType(type));
                skipItem.setLogicalName(obis);
                skipItem.setVersion(0);
                skipItem.setDescription(obis);
                List<ResultRead> ldt = new ArrayList<>();
                ldt = r.getRegister(skipItem);
                for (int j = 0; j < ldt.size(); j++) {
                    doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
                    doc.append("DATA", r.convertData(ldt.get(j)));
                }
                now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                doc.append("TGL_SELESAI", now.getTime());
                MongoDB.setData("DATA_GLOBAL", doc);
            } else {
                final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
                pg.setLogicalName(obis);
                pg.setObjectType(ObjectType.PROFILE_GENERIC);
                pg.setDescription(obis);

                GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
                pg2.setLogicalName(obis);
                pg2.setObjectType(ObjectType.PROFILE_GENERIC);
                pg2.setDescription(obis);

                r.getProfileGenericColumnsById(pg);

                if (pg.getCaptureObjects().size() > 0) {
                    final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                    if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName("0.0.1.0.0.255");
                        dt.setObjectType(ObjectType.DATA);
                        dt.setUIDataType(2, DataType.DATETIME);
                        pg2.addCaptureObject(dt, 2, 0);

                        dt = new GXDLMSObject();
                        dt.setLogicalName("1.0.0.8.4.255");
                        dt.setObjectType(ObjectType.DATA);
                        pg2.addCaptureObject(dt, 2, 0);

                        for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                            pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                    it.getValue().getDataIndex());
                        }
                    } else {
                        pg2 = pg;
                    }
                    final java.util.Calendar startLP = java.util.Calendar
                            .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    startLP.add(java.util.Calendar.DATE, -1);
                    final java.util.Calendar endLp = java.util.Calendar
                            .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    final List<Document> docs = r.getProfileGenericsMongo(pg2, column, startLP, endLp, UID, p);
                    MongoDB.setMultiData("DATA_" + "GLOBAL", docs);
                }
            }

        } catch (final Exception e) {

        }
    }

    public static void _DLMSreadData(final DLMS r, final String jenis, final String UID, final Params p) throws Exception {
        log(Level.INFO, "readData..." + jenis);
        final BasicDBObject query = new BasicDBObject();
        String jenisPhasa = "FASE1";
        if (p.getPhasa() == 1) {
            jenisPhasa = "FASE1";
        } else {
            jenisPhasa = "FASE3";
        }
        query.put(jenisPhasa, "Y");
        query.put(jenis, new BasicDBObject("$ne", 0).append("$exists", true));
        log(Level.INFO, "DATA_OBIS..." + jenis + ":" + jenisPhasa + " ==> ");
        /* Step 5 : Get all documents */
        final MongoDB db = new MongoDB();
        final List<DBObject> cursors = db.getData("DATA_OBIS", query, jenis);
        log(Level.INFO, "DATA_OBIS..." + jenis + " : " + cursors.size());
        if (cursors.size() == 0) {
            throw new NullPointerException("Obis tidak ditemukan");
        }
        /* Step 6 : Print all documents */
        java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
        System.out.println("Now  " + now.getTime());
        final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID", UID)
                .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER", p.getTypeMeter())
                .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
                .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT", p.getKdPembangkit())
                .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());

        for (final DBObject csr : cursors) {
            final String obis = csr.get("LOGICAL_NAME").toString();
            final String type = csr.get("CLASS_ID").toString();
            final String column = csr.get("NAMA_FIELD").toString();
            final GXObisCode skipItem = new GXObisCode();

            skipItem.setObjectType(r.getObjectType(type));

            skipItem.setLogicalName(obis);
            skipItem.setVersion(0);
            skipItem.setDescription(obis);
            List<ResultRead> ldt = new ArrayList<>();
            final String dt = null;
            ldt = r.getRegister(skipItem);

            for (int j = 0; j < ldt.size(); j++) {
                doc.remove(column + r.ifNull(ldt.get(j).getAlias()));
                doc.append(column + r.ifNull(ldt.get(j).getAlias()), r.convertData(ldt.get(j)));
            }
        }
        now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
        doc.append("TGL_SELESAI", now.getTime());
        MongoDB.setData("DATA_" + jenis, doc);
    }

    public static void _readDisconnectControl(final DLMS r, final String logicalName, final String classId, final String namaField,Document doc, final String UID, final Params p) {
        GXDLMSDisconnectControl control = new GXDLMSDisconnectControl(logicalName);
        control.setDescription("Disconnect Control");
        GXDLMSObject object = control;
        for (int pos : control.getAttributeIndexToRead(true)) {
            try {
                Object val = r.read(object, pos);
                switch (pos) {
                case 1:
                    control.setLogicalName(r.showValue(pos, val));
                    break;
                case 2:
                    if (r.showValue(pos, val).equals("true")) {
                        control.setOutputState(true);
                    } else {
                        control.setOutputState(false);
                    }
                    break;
                case 3:
                    if (r.showValue(pos, val).equals("Connected")) {
                        control.setControlState(ControlState.CONNECTED);
                    } else {
                        if (r.showValue(pos, val).equals("Disconnected")) {
                            control.setControlState(ControlState.DISCONNECTED);
                        } else {
                            control.setControlState(ControlState.READY_FOR_RECONNECTION);
                        }
                    }
                    break;
                case 4:
                    switch (r.showValue(pos, val)) {
                    case "None":
                        control.setControlMode(ControlMode.NONE);
                        break;
                    case "Mode1":
                        control.setControlMode(ControlMode.MODE_1);
                        break;
                    case "Mode2":
                        control.setControlMode(ControlMode.MODE_2);
                        break;
                    case "Mode3":
                        control.setControlMode(ControlMode.MODE_3);
                        break;
                    case "Mode4":
                        control.setControlMode(ControlMode.MODE_4);
                        break;
                    case "Mode5":
                        control.setControlMode(ControlMode.MODE_5);
                        break;
                    case "Mode6":
                        control.setControlMode(ControlMode.MODE_6);
                        break;
                    default:
                        control.setControlMode(ControlMode.NONE);
                        break;
                    }
                    break;
                }
            } catch (Exception ex) {
                System.out.println("error : " + ex.toString());
            }
        }
        // System.out.println("Logical Name = " + control.getLogicalName());
        // System.out.println("Output State = " + control.getOutputState());
        // System.out.println("Control State = " + control.getControlState());
        // System.out.println("Control Mode = " + control.getControlMode());
        doc.append("LOGICAL_NAME", control.getLogicalName().toString());
        doc.append("OUTPUT_STATE", control.getOutputState());
        doc.append("DATA", control.getControlState().toString());
        doc.append("CONTROL_MODE", control.getControlMode().toString());
        java.util.Calendar now = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
        doc.append("TGL_SELESAI", now.getTime());
        try {
            MongoDB.setData("DATA_" + "GLOBAL", doc);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void _DLMSreadProfileGeneric(final DLMS r, final String jenis, final java.util.Calendar startLP,
            final java.util.Calendar endLp, final String UID, final Params p) throws Exception {
        try {

            final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName("1.0.99.1.0.255");
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            pg.setDescription("LoadProfile");

            GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
            pg2.setLogicalName("1.0.99.1.0.255");
            pg2.setObjectType(ObjectType.PROFILE_GENERIC);
            pg2.setDescription("LoadProfile");

            final BasicDBObject query = new BasicDBObject();
            String jenisPhasa = "FASE1";
            if (p.getPhasa() == 1) {
                jenisPhasa = "FASE1";
            } else {
                jenisPhasa = "FASE3";
            }
            query.put(jenisPhasa, "Y");
            query.put("CLASS_ID", 7);
            String _jenis = "PROFILE_GENERIC";
            if (jenis.toUpperCase().equals("PROFILE")) {
                _jenis = "PROFILE_GENERIC";
            } else if (jenis.toUpperCase().equals("BILLING")) {
                _jenis = "BILLING_GENERIC";
            } else {
                _jenis = jenis;
            }
            query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));
            final MongoDB mg = new MongoDB();
            final List<DBObject> cursors = mg.getData("DATA_OBIS", query, _jenis);
            if (cursors.size() == 0) {
                throw new NullPointerException("Obis tidak ditemukan");
            }
            for (final DBObject csr : cursors) {
                final String obis = csr.get("LOGICAL_NAME").toString();
                final String type = csr.get("CLASS_ID").toString();
                final String column = csr.get("NAMA_FIELD").toString();
                final String deskripsi = csr.get("DESKRIPSI").toString();

                pg.setLogicalName(obis);
                pg.setObjectType(r.getObjectType(type));
                pg.setDescription(deskripsi);

                pg2.setLogicalName(obis);
                pg2.setObjectType(r.getObjectType(type));
                pg2.setDescription(deskripsi);

            }

            r.getProfileGenericColumnsById(pg);

            if (pg.getCaptureObjects().size() > 0) {
                final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                    GXDLMSObject dt = new GXDLMSObject();
                    dt.setLogicalName("0.0.1.0.0.255");
                    dt.setObjectType(ObjectType.DATA);
                    dt.setUIDataType(2, DataType.DATETIME);
                    pg2.addCaptureObject(dt, 2, 0);

                    dt = new GXDLMSObject();
                    dt.setLogicalName("1.0.0.8.4.255");
                    dt.setObjectType(ObjectType.DATA);
                    pg2.addCaptureObject(dt, 2, 0);

                    for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                        pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                it.getValue().getDataIndex());
                    }
                } else {
                    pg2 = pg;
                }
                final List<Document> docs = r.getProfileGenericsMongo(pg2, jenis, startLP, endLp, UID, p);
                MongoDB.setMultiData("DATA_" + jenis, docs);
            }
        } finally {
            r.close();
        }
    }
}
