package id.co.tabs.hes.dcu.reader;

import gurux.dlms.enums.DataType;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import id.co.tabs.hes.dlms.DLMS;

public class DLMSAction {
    public static void _DLMSdisconnectMeter(final DLMS r, final int value) throws Exception {
        System.out.println("Start Disconnect");
        final GXDLMSDisconnectControl dc = new GXDLMSDisconnectControl("0.0.96.3.10.255");
        // dc.setOutputState(true);
        final int ic = 1 + 0xff;
        r.readDLMSPacket(r.getClient().method(dc, value + 1, ic, DataType.INT8));
        System.out.println("Finish Disconnect");
    }
}
