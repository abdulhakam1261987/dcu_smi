package id.co.tabs.hes.dcu;

public interface IDCUManagerListener {
    void onDCUConnected(DCU dcu);
    void onDCUDisconnected(DCU dcu);
    void onDCUHeartbeat(DCU dcu);
}
