package id.co.tabs.hes.dcu.reader;

import java.text.SimpleDateFormat;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import gurux.dlms.enums.DataType;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.dlms.DLMS;

public class DLMSWriter {
    public static void _DLMSwriteMeter(final DLMS r, final String namaField, final String UID, final Params p,
            final String data) throws Exception {
        try {
            final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            final BasicDBObject query = new BasicDBObject();
            String jenisPhasa = "FASE1";
            if (p.getPhasa() == 1) {
                jenisPhasa = "FASE1";
            } else {
                jenisPhasa = "FASE3";
            }
            query.put(jenisPhasa, "Y");
            query.put("NAMA_FIELD", namaField);
            final MongoDB mg = new MongoDB();
            final List<DBObject> cursors = mg.getData("DATA_OBIS", query, null);
            if (cursors.size() == 0) {
                throw new NullPointerException("Obis tidak ditemukan");
            }
            String obis = "";
            String type = "";
            String column = "";
            String deskripsi = "";
            for (final DBObject csr : cursors) {
                obis = csr.get("LOGICAL_NAME").toString();
                type = csr.get("CLASS_ID").toString();
                column = csr.get("NAMA_FIELD").toString();
                deskripsi = csr.get("DESKRIPSI").toString();

                pg.setLogicalName(obis);
                pg.setObjectType(r.getObjectType(type));
                pg.setDescription(deskripsi);
            }
            Object data_ob = null;
            DataType dataType = null;
            if (type.equals("8")) {
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                final java.util.Calendar tgl = java.util.Calendar.getInstance();
                tgl.setTime(sdf.parse(data));
                // System.out.println("Tanggal "+data+ " :: "+tgl.getTime());
                data_ob = tgl.getTime();
                dataType = DataType.OCTET_STRING;
            } else if (type.equals("1")) {
                data_ob = data;
                dataType = DataType.STRING;
                // dataType = DataType.OCTET_STRING;
            } else if (type.equals("3")) {
                data_ob = data;
                dataType = DataType.UINT32;
            }
            if (data_ob != null) {
                r.readDLMSPacket(r.getClient().write(obis, data_ob, dataType, r.getObjectType(type), 2));
            }
        } finally {
            r.close();
        }
    }
}
