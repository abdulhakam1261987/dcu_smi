package id.co.tabs.hes.dcu;

import java.util.logging.Level;
import java.util.logging.Logger;

import gurux.common.GXCommon;
import gurux.common.IGXMediaListener;
import gurux.common.MediaStateEventArgs;
import gurux.common.PropertyChangedEventArgs;
import gurux.common.ReceiveEventArgs;
import gurux.common.TraceEventArgs;
import gurux.common.enums.MediaState;
import gurux.common.enums.TraceLevel;
import gurux.dlms.GXByteBuffer;
import gurux.dlms.GXDLMSClient;
import gurux.dlms.GXReplyData;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.Command;
import gurux.dlms.enums.InterfaceType;
import gurux.net.ConnectionEventArgs;
import gurux.net.GXNet;
import gurux.net.enums.NetworkType;
import id.co.tabs.hes.Settings;
import java.io.ByteArrayOutputStream;

public class Listener implements IGXMediaListener, gurux.net.IGXNetListener, AutoCloseable {

    private static final Logger LOGGER = Logger.getLogger(Listener.class.getName());

    /**
     * Are messages traced.
     */
    private Boolean trace = true;

    /**
     * TCP/IP port to listen.
     */
    private gurux.net.GXNet net;

    /**
     * Received data is saved to reply buffer because whole message is not always
     * received in one packet.
     */
    private GXByteBuffer reply = new GXByteBuffer();

    /**
     * Received data. This is used if GBT is used and data is received on several
     * data blocks.
     */
    private GXReplyData data = new GXReplyData();
    /**
     * Client used to parse received data.
     */
    private GXDLMSClient client = new GXDLMSClient(true, 0, 0, Authentication.NONE, null, InterfaceType.WRAPPER);

    public Listener(final int port) throws Exception {
        this.net = new gurux.net.GXNet(NetworkType.TCP, port);
        trace = Boolean.valueOf(Settings.getProperties().getProperty("listener.trace", "true"));
        if (trace) {
            this.net.setTrace(TraceLevel.VERBOSE);
        }
        this.net.addListener(this);
        onMediaStateChange(net, new MediaStateEventArgs(MediaState.CLOSED));
        this.net.open();
    }

    public void close() {
        this.net.close();
    }

    @Override
    public void onReceived(Object sender, ReceiveEventArgs e) {

        byte[] incoming = (byte[]) e.getData();
        try {
            DCU dcu = DCUManager.getInstance().getDCUByIP(e.getSenderInfo());

            if (dcu != null) {
                synchronized (dcu) {
                    // heartbeat got immediate response
                    if (DCUManager.getInstance().processHeartbeat(dcu, incoming))
                        return;
                    else if (DCUManager.getInstance().processHeartbeatSMI(dcu, incoming))
                        return;

                    if (dcu.isWaitingData()) {
                        dcu.packetReceived(incoming);
                        return;
                    }
                }
            }

            reply.set(incoming);
            // avoid invalid client address error
            client.setClientAddress(0);
            client.getData(reply, data);
            // If all data is received.
            if (data.isComplete() && !data.isMoreData()) {
                try {
                    // Handle meter registration messages
                    if (DCUManager.getInstance().isMeterRegistrationMessage(data)) {
                        LOGGER.info("WASION : Is Data Registration Message = TRUE");
                        DCUManager.getInstance().register((GXNet) sender, e.getSenderInfo(), data);
                    }else if (DCUManager.getInstance().isMeterRegistrationSMIMessage(incoming)) {
                        LOGGER.info("SMI : Is Data Registration Message = TRUE");
                        DCU dcus = DCUManager.getInstance().registerSMI((GXNet) sender, e.getSenderInfo(), incoming);
                        if(dcus!=null){
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            byte[] lnPushRegisterSMI = { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x16, (byte) 0xAA, 0x03 };
                            byte[] ENCRYP_SMI = { 0x00};

                            baos.write(lnPushRegisterSMI);
                            baos.write(dcus.getSNByte().length);
                            baos.write(dcus.getSNByte());
                            baos.write(ENCRYP_SMI);

                            byte[] CRC_SMI = DCUManager.getInstance().genCRCSMI(baos.toByteArray(), baos.toByteArray().length-0-9, 10, baos.toByteArray().length-0);

                            baos.write(CRC_SMI);
                            LOGGER.log(Level.INFO,
                        "Send Registered DCU with serial number " + dcus.getSerialNumber() + " at " + GXCommon.bytesToHex(baos.toByteArray()));
                            dcus.send(baos.toByteArray());
                        }else{
                            LOGGER.info("SMI : Is Data Registration Message = Lho Kok Null");
                        }
                    } else {
                        if ((dcu != null) && ((data.getCommand() == Command.EVENT_NOTIFICATION)
                                || (data.getCommand() == Command.INFORMATION_REPORT)
                                || (data.getCommand() == Command.DATA_NOTIFICATION))) {
                            dcu.handleNotifyMessages(data);
                        } else {
                            LOGGER.warning("Unhandled message: " + GXCommon.bytesToHex(incoming));
                        }
                    }
                } catch (Exception ex) {
                    logError(ex);
                } finally {
                    data.clear();
                }
            }
        } catch (Exception ex) {
            logError(ex);
            try{
            if (DCUManager.getInstance().isMeterRegistrationSMIMessage(incoming)) {
                LOGGER.info("SMI : Is Data Registration Message = TRUE");
                DCU dcus = DCUManager.getInstance().registerSMI((GXNet) sender, e.getSenderInfo(), incoming);
                if(dcus!=null){
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] lnPushRegisterSMI = { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x16, (byte) 0xAA, 0x03 };
                    byte[] ENCRYP_SMI = { 0x00};

                    baos.write(lnPushRegisterSMI);
                    baos.write(dcus.getSNByte().length);
                    baos.write(dcus.getSNByte());
                    baos.write(ENCRYP_SMI);

                    byte[] CRC_SMI = DCUManager.getInstance().genCRCSMI(baos.toByteArray(), baos.toByteArray().length-0-9, 10, baos.toByteArray().length-0);

                    baos.write(CRC_SMI);
                    LOGGER.log(Level.INFO,
                "Send Registered DCU with serial number " + dcus.getSerialNumber() + " at " + GXCommon.bytesToHex(baos.toByteArray()));
                    dcus.send(baos.toByteArray());
                }else{
                    LOGGER.info("SMI : Is Data Registration Message = Lho Kok Null");
                }
            }
        }catch(Exception ex1){
            logError(ex1);
        }finally{
            data.clear();
        }

        }
    }

    @Override
    public void onTrace(Object sender, TraceEventArgs e) {
        if (trace) {
            switch (e.getType()) {
            case RECEIVED:
                LOGGER.log(Level.INFO, "<- " + e.dataToString(false));
                break;
            case SENT:
                LOGGER.log(Level.INFO, "-> " + e.dataToString(false));
                break;
            case ERROR:
                LOGGER.log(Level.SEVERE, e.dataToString(true));
                break;
            case WARNING:
                LOGGER.log(Level.WARNING, e.dataToString(true));
            default:
            }
        }
    }

    // We do not need notify if property is changed.
    @Override
    public void onPropertyChanged(Object sender, PropertyChangedEventArgs e) {

    }

    @Override
    public void onMediaStateChange(Object sender, MediaStateEventArgs e) {

    }

    private void logError(Exception e) {
        LOGGER.log(Level.SEVERE, "An error has occured! " + e.getMessage());
    }

    /*
     * Client is made connection.
     */
    @Override
    public void onClientConnected(Object sender, ConnectionEventArgs e) {
        DCUManager.getInstance().connected(e.getInfo());
    }

    /*
     * Client is closed connection.
     */
    @Override
    public void onClientDisconnected(Object sender, ConnectionEventArgs e) {
        DCUManager.getInstance().disconnect(e.getInfo());
    }

    @Override
    public void onError(Object sender, Exception e) {
        try {
            logError(e);
        } catch (Exception ex) {
            logError(ex);
        }
    }
}
