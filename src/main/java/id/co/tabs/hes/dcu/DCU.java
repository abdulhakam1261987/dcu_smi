package id.co.tabs.hes.dcu;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.Map.Entry;

import gurux.common.AutoResetEvent;
import gurux.dlms.GXArray;
import gurux.dlms.GXDLMSGateway;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.GXStructure;
import gurux.common.GXCommon;
import gurux.dlms.GXDateTime;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Security;
import gurux.dlms.manufacturersettings.GXObisCode;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSObjectCollection;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.enums.ControlMode;
import gurux.dlms.objects.enums.ControlState;
import gurux.dlms.secure.GXDLMSSecureClient;
import gurux.net.GXNet;
import id.co.tabs.hes.DataLogicalName;
import id.co.tabs.hes.ListLogicalName;
import id.co.tabs.hes.MongoDB;
import id.co.tabs.hes.Params;
import id.co.tabs.hes.ResultRead;
import id.co.tabs.hes.Utils;
import id.co.tabs.hes.api.GetDataResponse;
import id.co.tabs.hes.dcu.reader.DLMSAction;
import id.co.tabs.hes.dcu.reader.DLMSReader;
import id.co.tabs.hes.dcu.reader.DLMSWriter;
import id.co.tabs.hes.dlms.DLMS;
import id.co.tabs.hes.dlms.IDLMSMedia;
import id.co.tabs.hes.dlms.object.Meter;
import id.co.tabs.hes.obis.descriptor.ObisDescriptor;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.text.NumberFormat;

public class DCU implements IDLMSMedia {

    private static final Logger LOGGER = Logger.getLogger(DCU.class.getName());
    private final byte[] heartbeatResponse = {0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00};
    private final byte[] heartbeatResponseSMI = {0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x16, (byte) 0x0A};
    private static final byte[] heartbeatSMICheck = {0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x15, 0x0C};

    private final int WaitTime = 40000;
    private final DLMS dlms = new DLMS(this);

    private GXNet media = null;
    GXDLMSGateway gateway = new GXDLMSGateway();
    private String ipAddress;
    private final String serialNumber;
    private String jenis;
    private byte[] snByte;

    private Boolean online;
    private Boolean waitingData = false;
    private String currentMeter = null;

    /**
     * Received event.
     */
    private final AutoResetEvent receivedEvent = new AutoResetEvent(false);
    private byte[] receivedData;

    public DCU(final GXNet media, final String ipAddress, final String serialNumber, final byte[] snByte, final String jenis) {
        this.media = media;
        this.ipAddress = ipAddress;
        this.serialNumber = serialNumber;
        this.jenis = jenis;
        this.snByte = snByte;
        this.dlms.setLogPrefix(getLogPrefix());
    }

    public String getJenis() {
        return jenis;
    }

    public byte[] getSNByte() {
        return snByte;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public Boolean getIsOnline() {
        return online;
    }

    public String getIPAddress() {
        return ipAddress;
    }

    public void setIPAddress(final String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setIsOnline(final Boolean online) {
        this.online = online;
    }

    private String getLogPrefix() {
        if (currentMeter == null) {
            return "DCU@" + this.serialNumber;
        } else {
            return "DCU@" + this.serialNumber + " Meter@" + currentMeter;
        }
    }

    private void log(final Level level, final String message) {
        LOGGER.log(level, getLogPrefix() + " " + message);
    }

    public Boolean isWaitingData() {
        return this.waitingData;
    }

    public void packetReceived(final byte[] packet) {
        receivedData = Arrays.copyOf(packet, packet.length);
        receivedEvent.set();
        waitingData = false;
    }

    @Override
    public void send(final Object data) {
        try {
            media.send(data, ipAddress);
        } catch (final Exception e) {
            log(Level.WARNING, e.getMessage());
            this.online = false;
        }
    }

    @Override
    public boolean receive() {
        try {
            waitingData = true;
            return receivedEvent.waitOne(WaitTime);
        } catch (final Exception ex) {
        }
        return false;
    }

    @Override
    public byte[] getReceivedData() {
        return receivedData;
    }

    public Boolean processHeartbeat(final byte[] packet) {
        if ((packet.length > 8) && (packet[7] == 3) && (packet[8] == 1)) {
            log(Level.INFO, "Incoming heartbeat, replying...");
            send(heartbeatResponse);
            return true;
        }
        return false;
    }

    public Boolean processHeartbeatSMI(final byte[] packet) {
        try {
            byte[] cekByte = new byte[9];
            for (int i = 0; i < 9; i++) {
                cekByte[i] = packet[i];
            }
            byte[] CRCSMI = DCUManager.getInstance().getCRCSMI(packet, 2);
            byte[] CRCSMI2 = DCUManager.getInstance().genCRCSMI(packet, packet.length - 9 - 2, 10, packet.length - 2);
            // System.out.println("HB=========>>>");
            // System.out.println(GXCommon.bytesToHex(cekByte));
            // System.out.println(GXCommon.bytesToHex(heartbeatSMICheck));
            // System.out.println("<<<=========HB");
            // System.out.println("CRC=========>>>");
            // System.out.println(GXCommon.bytesToHex(CRCSMI));
            // System.out.println(GXCommon.bytesToHex(CRCSMI2));
            // System.out.println("<<<=========CRC");
            if ((packet.length > 8) && Arrays.equals(cekByte, heartbeatSMICheck) && Arrays.equals(CRCSMI, CRCSMI2)) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] lnPushRegisterSMI = {0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x15, (byte) 0xCC, 0x03};

                //;{ (byte)0x50, (byte)0x42};
                baos.write(lnPushRegisterSMI);
                baos.write(snByte.length);
                baos.write(snByte);
                byte[] CRC_SMI = DCUManager.getInstance().genCRCSMI(baos.toByteArray(), baos.toByteArray().length - 9 - 0, 10, baos.toByteArray().length - 0);
                baos.write(CRC_SMI);
                log(Level.INFO, "Incoming heartbeat, replying...");
                System.out.println(GXCommon.bytesToHex(baos.toByteArray()));
                send(baos.toByteArray());
                return true;
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "An error Hearbeat has occured! " + e.getMessage());
        }
        return false;
    }

    public void setTargetMeter(final String serialNumber) {
        final GXDLMSSecureClient client = dlms.getClient();
        if ((serialNumber == null) || (serialNumber.isEmpty())) {
            log(Level.INFO, "Select DCU as target.");
            client.setPassword("00000000");
            client.setGateway(null);
            client.setClientAddress(1);
            currentMeter = null;
        } else {
            log(Level.INFO, "Select meter " + serialNumber + " as target.");
            gateway.setNetworkId((short) 0);
            gateway.setPhysicalDeviceAddress(serialNumber.getBytes());
            client.setPassword("00000000");
            client.setGateway(gateway);
            client.setClientAddress(5);
            currentMeter = serialNumber;
        }
        dlms.setLogPrefix(getLogPrefix());
    }

    public void setTargetMeter(final String serialNumber, final String AuthMeter) throws Exception {
        final HashMap<String, String> MapAuthMeter = MappingAuthMeter(AuthMeter);
        final GXDLMSSecureClient client = dlms.getClient();

        if ((serialNumber == null) || (serialNumber.isEmpty())) {
            log(Level.INFO, "Select DCU as target.");
            // client.setPassword("00000000");
            setParam(client, this.serialNumber, MapAuthMeter);
            // client =
            client.setGateway(null);
            // client.setClientAddress(1);
            currentMeter = null;
        } else {
            log(Level.INFO, "Select meter " + serialNumber + " as target.");
            gateway.setNetworkId((short) 0);
            gateway.setPhysicalDeviceAddress(serialNumber.getBytes());
            setParam(client, serialNumber, MapAuthMeter);
            // client.setPassword("00000000");
            client.setGateway(gateway);
            // client.setClientAddress(5);
            currentMeter = serialNumber;
        }
        dlms.setLogPrefix(getLogPrefix());
    }

    public String[] getMeterList() throws Exception {
        final GXDLMSObject obj = dlms.getObject("0.1.96.1.130.255");
        dlms.read(obj, 0x2);
        int i = 0;
        final GXArray ga = (GXArray) obj.getValues()[1];
        final String[] result = new String[ga.size()];
        for (i = 0; i < result.length; i++) {
            result[i] = ((String) ((GXStructure) ga.get(i)).get(1)).replaceFirst("^0+(?!$)", "");
        }
        return result;
    }

    public String[] getSMIDCUMeterList() throws Exception {
        final GXDLMSObject obj = dlms.getObject("0.0.96.50.21.255");
        dlms.read(obj, 0x2);
        int i = 0;
        final GXArray ga = (GXArray) obj.getValues()[1];
        final String[] result = new String[ga.size()];
        for (i = 0; i < result.length; i++) {
            result[i] = ((String) ((GXStructure) ga.get(i)).get(1)).replaceFirst("^0+(?!$)", "");
        }
        return result;
    }

    public void handleNotifyMessages(final GXReplyData reply) throws Exception {
        dlms.handleNotifyMessages(reply);
    }

    private boolean testReadExecuted = false;

    private void dumpObject(final String caption, final String ln, final int attributeIndex) throws Exception {
        log(Level.INFO, "Getting " + caption + "...");
        Object rest = dlms.read(dlms.getObject(ln), attributeIndex);
        log(Level.INFO, caption + ": " + rest == null ? "Null" : rest.toString());
    }

    public void testRead() {
        if (testReadExecuted) {
            return;
        }
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);
            dlms.setGateway(null);
            dlms.setClient(0);
            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            LOGGER.log(Level.INFO, "=======================================================");
            LOGGER.log(Level.INFO, "                         Read DCU Version");
            GXDLMSData versiDCU = new GXDLMSData("0.0.96.57.0.255");
            dlms.read(versiDCU, 2);
            LOGGER.log(Level.INFO, "Versi DCU : " + Utils.lihatNilai(2,
                    versiDCU.getValue().toString()));
            LOGGER.log(Level.INFO, "=======================================================");
            LOGGER.log(Level.INFO, "                         Read DCU Management Logical Device Name");
            GXDLMSData mLDN = new GXDLMSData("0.0.42.0.0.255");
            dlms.read(mLDN, 2);
            LOGGER.log(Level.INFO, "DCU logical device name : " + Utils.lihatNilai(2,
                    mLDN.getValue()));
            LOGGER.log(Level.INFO, "=======================================================");
            LOGGER.log(Level.INFO, "                         Read DCU Time");
            GXDLMSClock waktu = new GXDLMSClock("0.0.1.0.0.255");
            waktu.setVersion(0);
            dlms.read(waktu, 2);
            LOGGER.log(Level.INFO, "DCU time : " + waktu.getTime());
//            LOGGER.log(Level.INFO, "=======================================================");
//            LOGGER.log(Level.INFO, "                         Adjusting DCU Clock 1 Year Ahead");
//            Calendar calendar = waktu.getTime().getLocalCalendar();
//            calendar.add(Calendar.YEAR, 1);
//            waktu.setTime(calendar);
//            dlms.write(waktu, 2);
//            LOGGER.log(Level.INFO, "Adjusting DCU time finished");
//            LOGGER.log(Level.INFO, "=======================================================");
//            LOGGER.log(Level.INFO, "                         Read DCU Time");
//            dlms.read(waktu, 2);
//            LOGGER.log(Level.INFO, "DCU time after adjusting : " + waktu.getTime());
//            LOGGER.log(Level.INFO, "=======================================================");
//            LOGGER.log(Level.INFO, "                         Restoring DCU clock");
//            calendar = waktu.getTime().getLocalCalendar();
//            calendar.add(Calendar.YEAR, -1);
//            waktu.setTime(calendar);
//            dlms.write(waktu, 2);
//            LOGGER.log(Level.INFO, "Restoring DCU time finished");
//            LOGGER.log(Level.INFO, "=======================================================");
//            LOGGER.log(Level.INFO, "                         Read DCU Time");
//            dlms.read(waktu, 2);
//            LOGGER.log(Level.INFO, "DCU time after restoring : " + waktu.getTime());
            LOGGER.log(Level.INFO, "=======================================================");
            LOGGER.log(Level.INFO, "                         Read DCU Heartbeat Cycle");
            GXDLMSData hbCycle = new GXDLMSData("0.0.96.50.14.255");
            hbCycle.setVersion(0);
            dlms.read(hbCycle, 0x2);
            LOGGER.log(Level.INFO, "DCU heartbeat cycle : " + Utils.lihatNilai(2,
                    hbCycle.getValue()) + " detik");
            LOGGER.log(Level.INFO, "=======================================================");
            LOGGER.log(Level.INFO, "                         Read Meter List");
            GXDLMSData mP = new GXDLMSData("0.0.96.81.0.255");
            dlms.read(mP, 2);
            List<Meter> meters = objectToMeterList(mP.getValue());
            cetakMeterList(meters);
            client.disconnectRequest(true);
            for (Meter meter : meters) {
                try {
                    setTargetMeter(meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "Read Meter " + meter.getLogicalDeviceName());
                    GXDLMSGateway meterGateway = new GXDLMSGateway();
                    meterGateway.setPhysicalDeviceAddress(meter.getLogicalDeviceName().getBytes());
                    dlms.setClient(1);
                    dlms.setGateway(meterGateway);
                    dlms.initializeMeterConnection(meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "Read " + meter.getLogicalDeviceName() + " Meter ID");
                    GXDLMSData nomorMeter = new GXDLMSData("0.0.96.1.0.255");
                    nomorMeter.setVersion(0);
                    dlms.readMeter(nomorMeter, 0x2, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " ID : "
                            + Utils.lihatNilai(2, nomorMeter.getValue()));
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Meter Clock");
                    GXDLMSClock waktuMeter = new GXDLMSClock("0.0.1.0.0.255");
                    dlms.readMeter(waktuMeter, 0x2, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " time : " + waktuMeter.getTime());
                    LOGGER.log(Level.INFO, "=======================================================");
                    LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Limiter Threshold Active");
                    GXDLMSLimiter limiter = new GXDLMSLimiter("0.0.17.0.0.255");
                    dlms.readMeter(limiter, 0x3, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " limiter threshold active : " + limiter.getThresholdActive().toString());
                    LOGGER.log(Level.INFO, "======================================================= ");
                    LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Limiter Threshold Normal");
                    dlms.readMeter(limiter, 0x4, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " limiter threshold normal : " + limiter.getThresholdNormal().toString());
//            LOGGER.log(Level.INFO, "======================================================= ");
//            LOGGER.log(Level.INFO, "                         Set " + meter.getLogicalDeviceName() + " Limiter Threshold Normal to 1000");
//            limiter.setThresholdNormal(1000);
//            dlms.writeMeter(limiter, 4, meter.getLogicalDeviceName());
//            LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " write limiter threshold normal finished");
//            LOGGER.log(Level.INFO, "======================================================= ");
//            LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Limiter Threshold Normal After Changed");
//            dlms.readMeter(limiter, 0x4, meter.getLogicalDeviceName());
//            LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " limiter threshold normal : " + limiter.getThresholdNormal().toString());
//            LOGGER.log(Level.INFO, "======================================================= ");
//            LOGGER.log(Level.INFO, "                         Back " + meter.getLogicalDeviceName() + " Limiter Threshold Normal to its original value");
//            limiter.setThresholdNormal(0);
//            dlms.writeMeter(limiter, 4, meter.getLogicalDeviceName());
//            LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " write limiter threshold normal finished");
//            LOGGER.log(Level.INFO, "======================================================= ");
//            LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Limiter Threshold Normal After Changed");
//            dlms.readMeter(limiter, 0x4, meter.getLogicalDeviceName());
//            LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " limiter threshold normal : " + limiter.getThresholdNormal().toString());
//            LOGGER.log(Level.INFO, "                         Read " + meter.getLogicalDeviceName() + " Meter Association View");
//            dlms.getAssociationView(true, "Meter-" + this.currentMeter + ".xml");
                    LOGGER.log(Level.INFO, "======================================================= ");
                    LOGGER.log(Level.INFO, "Read " + meter.getLogicalDeviceName() + " Load profile");
                    GXDLMSProfileGeneric lP = new GXDLMSProfileGeneric("1.0.99.1.0.255");
                    lP.setVersion(1);
                    dlms.readMeter(lP, 7, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " load profile entries in use : "
                            + lP.getEntriesInUse());
                    dlms.readMeter(lP, 8, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " profile entries : " + lP.getProfileEntries());
                    dlms.readMeter(lP, 3, meter.getLogicalDeviceName());
                    dlms.addCollectionMeter(lP, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " load profile channel size : " + lP.getCaptureObjects().size());
                    int index = 1;
                    for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : lP.getCaptureObjects()) {
                        LOGGER.log(Level.INFO, "\t" + index + ".\t" + captureObject.getKey().getLogicalName() + "("
                                + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + ")");
                        index++;
                    }
                    Calendar start = waktuMeter.getTime().getLocalCalendar();
                    start.set(Calendar.HOUR_OF_DAY, 0);
                    start.set(Calendar.MINUTE, 0);
                    start.set(Calendar.SECOND, 0);
                    Calendar end = waktuMeter.getTime().getLocalCalendar();
                    dlms.readRowsByRangeMeter(lP, start, end, meter.getLogicalDeviceName());
                    dlms.printProfileGenericBuffer(lP);
                    LOGGER.log(Level.INFO, "======================================================= ");
                    LOGGER.log(Level.INFO, "Read " + meter.getLogicalDeviceName() + " End of Billing");
                    GXDLMSProfileGeneric eob = new GXDLMSProfileGeneric("0.0.98.1.0.255");
                    eob.setVersion(1);
                    dlms.readMeter(eob, 7, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " end of billing entries in use : " + eob.getEntriesInUse());
                    dlms.readMeter(eob, 8, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " end of billing entries : " + eob.getProfileEntries());
                    dlms.readMeter(eob, 3, meter.getLogicalDeviceName());
                    LOGGER.log(Level.INFO, "Meter " + meter.getLogicalDeviceName() + " end of billing channel size : " + eob.getCaptureObjects().size());
                    index = 1;
                    dlms.addCollectionMeter(eob, meter.getLogicalDeviceName());
                    for (Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : eob.getCaptureObjects()) {
                        LOGGER.log(Level.INFO, "\t" + index + ".\t" + captureObject.getKey().getLogicalName() + "("
                                + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + ")");
                        index++;
                    }
                    dlms.readMeter(eob, 2, meter.getLogicalDeviceName());
                    dlms.printProfileGenericBuffer(eob);
                } catch (Exception exception) {
                    LOGGER.log(Level.SEVERE, meter.getLogicalDeviceName() + " : " + exception.getMessage());
                } finally {
                    client.disconnectRequest(true);
                }
            }
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            LOGGER.log(Level.SEVERE, e.toString());
            dlms.setGateway(null);
//            e.printStackTrace();
            testReadExecuted = false;
        }
    }

    List<Meter> objectToMeterList(Object listMeter) {
        List<Meter> meters = new ArrayList<>();
        for (Object tmp : (List<?>) listMeter) {
            Meter meter = Meter.parseMeterFromObject((List) tmp);
            meters.add(meter);
        }
        return meters;
    }

    void cetakMeterList(List<Meter> meters) {
        LOGGER.log(Level.INFO, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        LOGGER.log(Level.INFO, "                         METER LIST");
        for (Meter meter : meters) {
            LOGGER.log(Level.INFO, "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            LOGGER.log(Level.INFO, meter.toString());
        }
    }

    String kodeMeterParameter(int kode) {
        switch (kode) {
            case 1:
                return "\tEnumeration number : ";
            case 2:
                return "\tLogical device name : ";
            case 3:
                return "\tPort code : ";
            case 4:
                return "\tBaud rate code : ";
            case 5:
                return "\tCommunication protocol type : ";
            case 6:
                return "\tData bit length : ";
            case 7:
                return "\tData transmission start bit : ";
            case 8:
                return "\tData transmission stop bit : ";
            case 9:
                return "\tData transmission check bit : ";
            case 10:
                return "\tData transparent meter tariff : ";
            case 11:
                return "\tMeter wiring code : ";
            default:
                return "\tUnknown : ";
        }
    }

    public List<String> ReadListMeter(final String AuthMeter) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeter);

            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            log(Level.INFO, "Login Start ...." + this.serialNumber);
            try {
                dlms.initializeConnection();
            } catch (Exception e) {
                log(Level.SEVERE, "Login End ...." + e.getMessage());
            }
            log(Level.INFO, "Login End ...." + this.serialNumber);

            // dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            GXObisCode skipItem = new GXObisCode();
            skipItem.setObjectType(ObjectType.CLOCK);
            skipItem.setLogicalName("0.0.1.0.0.255");
            skipItem.setVersion(0);
            skipItem.setDescription("0.0.1.0.0.255");

            // GXDLMSClock obj1 = new GXDLMSClock();
            // obj1.setObjectType((ObjectType) skipItem.getObjectType());
            // obj1.setLogicalName(skipItem.getLogicalName());
            // obj1.setDescription(skipItem.getDescription());
            GXDLMSObject obj = objFromCode(skipItem);
            Object rest = dlms.read(obj, 2);
            log(Level.INFO, "Clock..." + ": " + rest == null ? "Null" : rest.toString());

            obj = new GXDLMSObject();

            skipItem = new GXObisCode();
            skipItem.setObjectType(ObjectType.forValue(1));
            skipItem.setLogicalName("0.0.96.81.0.255");
            skipItem.setVersion(0);
            skipItem.setDescription("0.0.96.81.0.255");

            obj = objFromCode(skipItem);

            // GXDLMSObject obj = obj1;
            rest = dlms.read(obj, 2);
            log(Level.INFO, "0.0.96.81.0.255..." + ": " + rest == null ? "Null" : rest.toString());
//            dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);

            // log(Level.INFO, "Getting Meter list...");
            final String[] meterList = new String[1];//getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));

            // log(Level.INFO, "======= M E T E R T E S T =======");
            client.disconnectRequest(true);
            testReadExecuted = false;
            return result("Sukses", Arrays.toString(meterList), 1);

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadClock(final String noMeter, final String AuthMeterMeter, final String AuthMeterDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            // setTargetMeter(null);
            setTargetMeter(null, AuthMeterDCU);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;

            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            client.disconnectRequest(true);

            GXDLMSObject obj;
            // setTargetMeter(noMeter);
            setTargetMeter(noMeter, AuthMeterMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                // log(Level.INFO, "Getting meter clock...");
                obj = client.getObjects().findByLN(ObjectType.CLOCK, "0.0.1.0.0.255");
                final GXDLMSClock clock = (GXDLMSClock) obj;
                log(Level.INFO, "=======  M E T E R   R E A D   C L O C K  =======");
                dlms.read(obj, 0x2);
                log(Level.INFO, "Initial meter clock: " + clock.getTime().toString());
                // rslt = sdf.format(clock.getTime().getLocalCalendar());
                //
                rslt = clock.getTime().toString();
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                // client.disconnectRequest(true);
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> writeClock(final String noMeter, final String tanggal, final String AuthMeterMeter,
            final String AuthMeterDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null, AuthMeterDCU);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;

            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            log(Level.INFO, "=======  M E T E R    W R I T E  =======");
            log(Level.INFO, "Write Clock " + tanggal);
            client.disconnectRequest(true);

            GXDLMSObject obj;
            setTargetMeter(noMeter, AuthMeterMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                dlms.initializeConnection();
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "Getting meter clock...");
                obj = client.getObjects().findByLN(ObjectType.CLOCK, "0.0.1.0.0.255");
                final GXDLMSClock clock = (GXDLMSClock) obj;

                log(Level.INFO, "Adjusting meter clock...");
                final Calendar calendar = clock.getTime().getLocalCalendar();
                final SimpleDateFormat sdfN = new SimpleDateFormat("yyyyMMddHHmmss");
                calendar.setTime(sdfN.parse(tanggal));
                clock.setTime(calendar);
                dlms.write(clock, 0x2);
                log(Level.INFO, "Requery meter clock...");
                dlms.read(obj, 0x2);
                log(Level.INFO, "Adjusted meter clock: " + clock.getTime().toString());
                // rslt = sdf.format(clock.getTime().getLocalCalendar());
                rslt = clock.getTime().toString();
            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                return result("Sukses", rslt, 1);
            }

        } catch (final Exception e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> ReadLoadProfile(final String noMeter, final String tanggalAwal, final String tanggalAkhir,
            final String AuthMeter, final String AuthDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        List<JSONObject> Ljson = new ArrayList<JSONObject>();
        try {
            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            setTargetMeter(null, AuthDCU);
            client.disconnectRequest(true);

            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;

            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            // log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));
            log(Level.INFO, "=======  M E T E R    R E A D  =======");
            log(Level.INFO, "Read Load Profile from " + tanggalAwal + " to " + tanggalAkhir);
            client.disconnectRequest(true);

            // GXDLMSObject obj;
            setTargetMeter(noMeter, AuthMeter);
            client.disconnectRequest(true);
            try {
                dlms.initializeConnection();

                log(Level.INFO, "Getting load Profile...");
                final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
                pg.setLogicalName("1.0.99.1.0.255");
                pg.setObjectType(ObjectType.PROFILE_GENERIC);
                pg.setDescription("LoadProfile");

                GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
                pg2.setLogicalName("1.0.99.1.0.255");
                pg2.setObjectType(ObjectType.PROFILE_GENERIC);
                pg2.setDescription("LoadProfile");

                dlms.getProfileGenericColumnsById(pg);
                if (pg.getCaptureObjects().size() > 0) {
                    final Entry<GXDLMSObject, GXDLMSCaptureObject> itz = pg.getCaptureObjects().get(0);

                    if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName("0.0.1.0.0.255");
                        dt.setObjectType(ObjectType.DATA);
                        dt.setUIDataType(2, DataType.DATETIME);
                        pg2.addCaptureObject(dt, 2, 0);

                        dt = new GXDLMSObject();
                        dt.setLogicalName("1.0.0.8.4.255");
                        dt.setObjectType(ObjectType.DATA);
                        pg2.addCaptureObject(dt, 2, 0);

                        for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it : pg.getCaptureObjects()) {
                            pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
                                    it.getValue().getDataIndex());
                        }
                    } else {
                        pg2 = pg;
                    }

                    final Calendar startLP = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    final Calendar endLp = Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
                    startLP.setTime(sdf.parse(tanggalAwal));
                    endLp.setTime(sdf.parse(tanggalAkhir));
                    Ljson = dlms.getProfileGenericsByDates(pg2, startLP, endLp);
                }
            } finally {
                client.disconnectRequest(true);
                // testReadExecuted = false;
                dlms.close();
                return result("Sukses", Ljson, 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    public List<String> disconnectReconnect(final String noMeter, final int status, final String AuthMeter,
            final String AuthDCU) {
        if (testReadExecuted) {
            return result("Proses Penarikan Data", "", 0);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null, AuthDCU);

            log(Level.INFO, "=======  D C U    T E S T  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");
            // ..readScalerAndUnits();
            // GXDLMSObject obj;

            // dumpObject("DCU clock", "0.0.1.0.0.255", 0x2);
            // dumpObject("DCU firmware version", "1.0.0.2.0.255", 0x2);
            log(Level.INFO, "Getting Meter list...");
            // String[] meterList = getMeterList();
            // log(Level.INFO, "Meter List: " + Arrays.toString(meterList));

            // log(Level.INFO, "======= M E T E R T E S T =======");
            client.disconnectRequest(true);

            setTargetMeter(noMeter, AuthMeter);
            client.disconnectRequest(true);
            try {
                dlms.initializeConnection();
                log(Level.INFO, "Getting Disconnect Reconnect...");
                final GXDLMSDisconnectControl dc = new GXDLMSDisconnectControl("0.0.96.3.10.255");
                final byte[][] b = client.method(dc, status + 1, 1 + 0xff, DataType.INT8);
                dlms.readDLMSPacket(b);
                log(Level.INFO, "Finish Disconnect Reconnect...");

            } finally {
                // client.disconnectRequest(true);
                dlms.close();
                testReadExecuted = false;
                return result("Sukses", "", 1);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), "", 0);
        }
    }

    private List<String> result(final String msg, final Object data, final int out) {
        // JSONObject json = new JSONObject();
        // json.put("msg", msg);
        // json.put("data", data);
        // json.put("out", out);
        final List<String> result = new ArrayList<>();
        result.add(msg);
        result.add(data.toString());
        result.add(Integer.toString(out));
        return result;
    }

    private GetDataResponse result(final String msg, final int out, final String UID, final String jenis) {
        // JSONObject json = new JSONObject();
        // json.put("msg", msg);
        // json.put("data", data);
        // json.put("out", out);
        List<DBObject> l_rslt = new ArrayList<>();
        String rslt = "";
        if (msg.equals("Sukses")) {
            BasicDBObject query = new BasicDBObject();
            query = new BasicDBObject();
            query.put("UID", UID);
            final MongoDB mg = new MongoDB();
            try {
                l_rslt = mg.getData("DATA_" + jenis, query, "");
            } catch (final Exception e) {
                e.printStackTrace();
            }

        }

        final Gson gson = new Gson();
        rslt = gson.toJson(l_rslt);
        // System.out.println("=================== R E S U L T M O N G O - D B");
        // System.out.println(l_rslt);
        final GetDataResponse resp = GetDataResponse.newBuilder().setMessages(msg).setResult(rslt.toString())
                .setOut(Integer.toString(out)).build();
        return resp;

        // result.add(msg);
        // result.add(data.toString());
        // result.add(Integer.toString(out));
        // return result;
    }

    // -------------------------
    public GetDataResponse readOneData(final String noMeter, final String logicalName, final String classId,
            final String fieldName, final String UID, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D    O N E   D A T A  =======");
                DLMSReader._DLMSreadOneData(dlms, logicalName, classId, fieldName, UID, p);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");

                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GetDataResponse readData(final String noMeter, final String jenis, final String UID, final Params p) {
        System.out.println("readData ..... ");
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        System.out.println("readData ..... ");
        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                DLMSReader._DLMSreadData(dlms, jenis, UID, p);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse readDataProfile(final String noMeter, final String jenis, final java.util.Calendar startLP,
            final java.util.Calendar endLp, final String UID, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                // final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                // sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   P R O F I L E  =======");
                log(Level.INFO, " From : " + startLP.getTime() + " To : " + endLp.getTime());
                DLMSReader._DLMSreadProfileGeneric(dlms, jenis, startLP, endLp, UID, p);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse writeData(final String noMeter, final String jenis, final String UID, final Params p,
            final String value) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   W R I T E   D A T A  =======");
                DLMSWriter._DLMSwriteMeter(dlms, jenis, UID, p, value);
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   W R I T E   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    public GetDataResponse actionData(final String noMeter, final String jenis, final String UID, final Params p,
            final String value) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, jenis);
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   A C T I O N  =======");
                DLMSAction._DLMSdisconnectMeter(dlms, Integer.parseInt(value));
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   A C T I O N   E N D  =======");
                return result("Sukses", 1, UID, jenis);
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, jenis);
        }
    }

    // ---------------
    // public void _DLMSreadData(final DLMS r, final String jenis, final String UID,
    // final Params p) throws Exception {
    // log(Level.INFO, "readData..." + jenis);
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put(jenis, new BasicDBObject("$ne", 0).append("$exists", true));
    // log(Level.INFO, "DATA_OBIS..." + jenis + ":" + jenisPhasa + " ==> ");
    // /* Step 5 : Get all documents */
    // final MongoDB db = new MongoDB();
    // final List<DBObject> cursors = db.getData("DATA_OBIS", query, jenis);
    // log(Level.INFO, "DATA_OBIS..." + jenis + " : " + cursors.size());
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // /* Step 6 : Print all documents */
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // System.out.println("Now " + now.getTime());
    // final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID",
    // UID)
    // .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER",
    // p.getTypeMeter())
    // .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
    // .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT",
    // p.getKdPembangkit())
    // .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());
    // for (final DBObject csr : cursors) {
    // final String obis = csr.get("LOGICAL_NAME").toString();
    // final String type = csr.get("CLASS_ID").toString();
    // final String column = csr.get("NAMA_FIELD").toString();
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // final String dt = null;
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.remove(column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append(column + r.ifNull(ldt.get(j).getAlias()),
    // r.convertData(ldt.get(j)));
    // }
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_" + jenis, doc);
    // }
    // void _DLMSreadOneData(final DLMS r, final String logicalName, final String
    // classId, final String namaField,
    // final String UID, final Params p) {
    // try {
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // final Document doc = new Document("TGL_MULAI", now.getTime()).append("UID",
    // UID)
    // .append("MERK_METER", p.getMerkMeter()).append("TYPE_METER",
    // p.getTypeMeter())
    // .append("NO_METER", p.getNoMeter()).append("KD_PUSAT", p.getKdPusat())
    // .append("KD_UNIT_BISNIS", p.getKdUnitBisnis()).append("KD_PEMBANGKIT",
    // p.getKdPembangkit())
    // .append("KD_AREA", p.getKdArea()).append("JENIS", p.getJenis());
    // final String obis = logicalName;
    // final String type = classId;
    // final String column = namaField;
    // if(classId.equals("70")){
    // _readDisconnectControl(r,logicalName, classId, namaField, doc, UID, p);
    // }else if (!classId.equals("8")) {
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append("DATA", r.convertData(ldt.get(j)));
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_GLOBAL", doc);
    // }
    // else if (!classId.equals("7")) {
    // final GXObisCode skipItem = new GXObisCode();
    // skipItem.setObjectType(r.getObjectType(type));
    // skipItem.setLogicalName(obis);
    // skipItem.setVersion(0);
    // skipItem.setDescription(obis);
    // List<ResultRead> ldt = new ArrayList<>();
    // ldt = r.getRegister(skipItem);
    // for (int j = 0; j < ldt.size(); j++) {
    // doc.append("NAMA_FIELD", column + r.ifNull(ldt.get(j).getAlias()));
    // doc.append("DATA", r.convertData(ldt.get(j)));
    // }
    // now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // MongoDB.setData("DATA_GLOBAL", doc);
    // } else {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // pg.setLogicalName(obis);
    // pg.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg.setDescription(obis);
    // GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
    // pg2.setLogicalName(obis);
    // pg2.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg2.setDescription(obis);
    // r.getProfileGenericColumnsById(pg);
    // if (pg.getCaptureObjects().size() > 0) {
    // final Entry<GXDLMSObject, GXDLMSCaptureObject> itz =
    // pg.getCaptureObjects().get(0);
    // if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
    // GXDLMSObject dt = new GXDLMSObject();
    // dt.setLogicalName("0.0.1.0.0.255");
    // dt.setObjectType(ObjectType.DATA);
    // dt.setUIDataType(2, DataType.DATETIME);
    // pg2.addCaptureObject(dt, 2, 0);
    // dt = new GXDLMSObject();
    // dt.setLogicalName("1.0.0.8.4.255");
    // dt.setObjectType(ObjectType.DATA);
    // pg2.addCaptureObject(dt, 2, 0);
    // for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it :
    // pg.getCaptureObjects()) {
    // pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
    // it.getValue().getDataIndex());
    // }
    // } else {
    // pg2 = pg;
    // }
    // final java.util.Calendar startLP = java.util.Calendar
    // .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // startLP.add(java.util.Calendar.DATE, -1);
    // final java.util.Calendar endLp = java.util.Calendar
    // .getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // final List<Document> docs = r.getProfileGenericsMongo(pg2, column, startLP,
    // endLp, UID, p);
    // MongoDB.setMultiData("DATA_" + "GLOBAL", docs);
    // }
    // }
    // } catch (final Exception e) {
    // }
    // }
    // void _DLMSwriteMeter(final DLMS r, final String namaField, final String UID,
    // final Params p, final String data)
    // throws Exception {
    // try {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put("NAMA_FIELD", namaField);
    // final MongoDB mg = new MongoDB();
    // final List<DBObject> cursors = mg.getData("DATA_OBIS", query, null);
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // String obis = "";
    // String type = "";
    // String column = "";
    // String deskripsi = "";
    // for (final DBObject csr : cursors) {
    // obis = csr.get("LOGICAL_NAME").toString();
    // type = csr.get("CLASS_ID").toString();
    // column = csr.get("NAMA_FIELD").toString();
    // deskripsi = csr.get("DESKRIPSI").toString();
    // pg.setLogicalName(obis);
    // pg.setObjectType(r.getObjectType(type));
    // pg.setDescription(deskripsi);
    // }
    // Object data_ob = null;
    // DataType dataType = null;
    // if (type.equals("8")) {
    // final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    // final java.util.Calendar tgl = java.util.Calendar.getInstance();
    // tgl.setTime(sdf.parse(data));
    // // System.out.println("Tanggal "+data+ " :: "+tgl.getTime());
    // data_ob = tgl.getTime();
    // dataType = DataType.OCTET_STRING;
    // } else if (type.equals("1")) {
    // data_ob = data;
    // dataType = DataType.STRING;
    // // dataType = DataType.OCTET_STRING;
    // } else if (type.equals("3")) {
    // data_ob = data;
    // dataType = DataType.UINT32;
    // }
    // if (data_ob != null) {
    // r.readDLMSPacket(r.getClient().write(obis, data_ob, dataType,
    // r.getObjectType(type), 2));
    // }
    // } finally {
    // r.close();
    // }
    // }
    // public void _DLMSreadProfileGeneric(final DLMS r, final String jenis, final
    // java.util.Calendar startLP,
    // final java.util.Calendar endLp, final String UID, final Params p) throws
    // Exception {
    // try {
    // final GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
    // pg.setLogicalName("1.0.99.1.0.255");
    // pg.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg.setDescription("LoadProfile");
    // GXDLMSProfileGeneric pg2 = new GXDLMSProfileGeneric();
    // pg2.setLogicalName("1.0.99.1.0.255");
    // pg2.setObjectType(ObjectType.PROFILE_GENERIC);
    // pg2.setDescription("LoadProfile");
    // final BasicDBObject query = new BasicDBObject();
    // String jenisPhasa = "FASE1";
    // if (p.getPhasa() == 1) {
    // jenisPhasa = "FASE1";
    // } else {
    // jenisPhasa = "FASE3";
    // }
    // query.put(jenisPhasa, "Y");
    // query.put("CLASS_ID", 7);
    // String _jenis = "PROFILE_GENERIC";
    // if (jenis.toUpperCase().equals("PROFILE")) {
    // _jenis = "PROFILE_GENERIC";
    // } else if (jenis.toUpperCase().equals("BILLING")) {
    // _jenis = "BILLING_GENERIC";
    // } else {
    // _jenis = jenis;
    // }
    // query.put(_jenis, new BasicDBObject("$ne", 0).append("$exists", true));
    // final MongoDB mg = new MongoDB();
    // final List<DBObject> cursors = mg.getData("DATA_OBIS", query, _jenis);
    // if (cursors.size() == 0) {
    // throw new NullPointerException("Obis tidak ditemukan");
    // }
    // for (final DBObject csr : cursors) {
    // final String obis = csr.get("LOGICAL_NAME").toString();
    // final String type = csr.get("CLASS_ID").toString();
    // final String column = csr.get("NAMA_FIELD").toString();
    // final String deskripsi = csr.get("DESKRIPSI").toString();
    // pg.setLogicalName(obis);
    // pg.setObjectType(r.getObjectType(type));
    // pg.setDescription(deskripsi);
    // pg2.setLogicalName(obis);
    // pg2.setObjectType(r.getObjectType(type));
    // pg2.setDescription(deskripsi);
    // }
    // r.getProfileGenericColumnsById(pg);
    // if (pg.getCaptureObjects().size() > 0) {
    // final Entry<GXDLMSObject, GXDLMSCaptureObject> itz =
    // pg.getCaptureObjects().get(0);
    // if (!itz.getKey().getLogicalName().contains("0.0.1.0.0.255")) {
    // GXDLMSObject dt = new GXDLMSObject();
    // dt.setLogicalName("0.0.1.0.0.255");
    // dt.setObjectType(ObjectType.DATA);
    // dt.setUIDataType(2, DataType.DATETIME);
    // pg2.addCaptureObject(dt, 2, 0);
    // dt = new GXDLMSObject();
    // dt.setLogicalName("1.0.0.8.4.255");
    // dt.setObjectType(ObjectType.DATA);
    // pg2.addCaptureObject(dt, 2, 0);
    // for (final Entry<GXDLMSObject, GXDLMSCaptureObject> it :
    // pg.getCaptureObjects()) {
    // pg2.addCaptureObject(it.getKey(), it.getValue().getAttributeIndex(),
    // it.getValue().getDataIndex());
    // }
    // } else {
    // pg2 = pg;
    // }
    // final List<Document> docs = r.getProfileGenericsMongo(pg2, jenis, startLP,
    // endLp, UID, p);
    // MongoDB.setMultiData("DATA_" + jenis, docs);
    // }
    // } finally {
    // r.close();
    // }
    // }
    // void _DLMSdisconnectMeter(final DLMS r, final int value) throws Exception {
    // System.out.println("Start Disconnect");
    // final GXDLMSDisconnectControl dc = new
    // GXDLMSDisconnectControl("0.0.96.3.10.255");
    // // dc.setOutputState(true);
    // final int ic = 1 + 0xff;
    // r.readDLMSPacket(r.getClient().method(dc, value + 1, ic, DataType.INT8));
    // System.out.println("Finish Disconnect");
    // }
    // public void _readDisconnectControl(final DLMS r, final String logicalName,
    // final String classId, final String namaField,Document doc, final String UID,
    // final Params p) {
    // GXDLMSDisconnectControl control = new GXDLMSDisconnectControl(logicalName);
    // control.setDescription("Disconnect Control");
    // GXDLMSObject object = control;
    // for (int pos : control.getAttributeIndexToRead(true)) {
    // try {
    // Object val = r.read(object, pos);
    // switch (pos) {
    // case 1:
    // control.setLogicalName(r.showValue(pos, val));
    // break;
    // case 2:
    // if (r.showValue(pos, val).equals("true")) {
    // control.setOutputState(true);
    // } else {
    // control.setOutputState(false);
    // }
    // break;
    // case 3:
    // if (r.showValue(pos, val).equals("Connected")) {
    // control.setControlState(ControlState.CONNECTED);
    // } else {
    // if (r.showValue(pos, val).equals("Disconnected")) {
    // control.setControlState(ControlState.DISCONNECTED);
    // } else {
    // control.setControlState(ControlState.READY_FOR_RECONNECTION);
    // }
    // }
    // break;
    // case 4:
    // switch (r.showValue(pos, val)) {
    // case "None":
    // control.setControlMode(ControlMode.NONE);
    // break;
    // case "Mode1":
    // control.setControlMode(ControlMode.MODE_1);
    // break;
    // case "Mode2":
    // control.setControlMode(ControlMode.MODE_2);
    // break;
    // case "Mode3":
    // control.setControlMode(ControlMode.MODE_3);
    // break;
    // case "Mode4":
    // control.setControlMode(ControlMode.MODE_4);
    // break;
    // case "Mode5":
    // control.setControlMode(ControlMode.MODE_5);
    // break;
    // case "Mode6":
    // control.setControlMode(ControlMode.MODE_6);
    // break;
    // default:
    // control.setControlMode(ControlMode.NONE);
    // break;
    // }
    // break;
    // }
    // } catch (Exception ex) {
    // System.out.println("error : " + ex.toString());
    // }
    // }
    // // System.out.println("Logical Name = " + control.getLogicalName());
    // // System.out.println("Output State = " + control.getOutputState());
    // // System.out.println("Control State = " + control.getControlState());
    // // System.out.println("Control Mode = " + control.getControlMode());
    // doc.append("LOGICAL_NAME", control.getLogicalName().toString());
    // doc.append("OUTPUT_STATE", control.getOutputState());
    // doc.append("DATA", control.getControlState().toString());
    // doc.append("CONTROL_MODE", control.getControlMode().toString());
    // java.util.Calendar now =
    // java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone("GMT+7:00"));
    // doc.append("TGL_SELESAI", now.getTime());
    // try {
    // MongoDB.setData("DATA_" + "GLOBAL", doc);
    // } catch (Exception e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }
    public GetDataResponse getMultiLogicalName(final ListLogicalName l_logicalName, final String UID,
            final String noMeter, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                for (final DataLogicalName ln : l_logicalName.getListObis()) {
                    DLMSReader._DLMSreadOneData(dlms, ln.getLogicalName(), ln.getClassId(), ln.getLn(), UID, p);
                }
            } catch (final Exception e) {
                e.printStackTrace();
                // rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GetDataResponse setMultiLogicalName(final ListLogicalName l_logicalName, final String UID,
            final String noMeter, final Params p) {
        if (testReadExecuted) {
            return result("Proses Penarikan", 0, UID, "GLOBAL");
        }

        final GXDLMSSecureClient client = dlms.getClient();
        testReadExecuted = true;
        try {
            setTargetMeter(null);

            log(Level.INFO, "=======  D C U    C O N N E C T I O N  =======");
            client.disconnectRequest(true);
            dlms.initializeConnection();

            dlms.getAssociationView(true, "DCU-" + this.serialNumber + ".xml");

            client.disconnectRequest(true);
            setTargetMeter(noMeter);
            client.disconnectRequest(true);
            String rslt = "";
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
                log(Level.INFO, "=======  M E T E R   L O G I N  =======");
                dlms.initializeConnection();

                log(Level.INFO, "=======  M E T E R   R E A D   D A T A  =======");
                for (final DataLogicalName ln : l_logicalName.getListObis()) {
                    DLMSWriter._DLMSwriteMeter(dlms, ln.getLn(), UID, p, ln.getVal());
                }
            } catch (final Exception e) {
                e.printStackTrace();
                rslt = e.getMessage();
            } finally {
                log(Level.INFO, "=======  C L O S E   C O N N E C T I O N  =======");
                dlms.close();
                testReadExecuted = false;
                log(Level.INFO, "=======  M E T E R   R E A D   E N D  =======");
                return result("Sukses", 1, UID, "GLOBAL");
            }

        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            testReadExecuted = false;
            return result(e.getMessage(), 0, UID, "GLOBAL");
        }
    }

    public GXDLMSObject objFromCode(GXObisCode code) {
        if (null != code.getObjectType()) {
            switch (code.getObjectType()) {
                case CLOCK: {
                    GXDLMSClock obj = new GXDLMSClock();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case DATA: {
                    GXDLMSData obj = new GXDLMSData();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case REGISTER: {
                    GXDLMSRegister obj = new GXDLMSRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;

                }
                case EXTENDED_REGISTER: {
                    GXDLMSExtendedRegister obj = new GXDLMSExtendedRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case DEMAND_REGISTER: {
                    GXDLMSDemandRegister obj = new GXDLMSDemandRegister();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                case PROFILE_GENERIC: {
                    GXDLMSProfileGeneric obj = new GXDLMSProfileGeneric();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
                default: {
                    GXDLMSObject obj = new GXDLMSObject();
                    obj.setObjectType((ObjectType) code.getObjectType());
                    obj.setLogicalName(code.getLogicalName());
                    obj.setDescription(code.getDescription());
                    return obj;
                }
            }
        } else {
            return null;
        }
    }

    static Params setParam(final GXDLMSSecureClient client, final String nometer,
            final HashMap<String, String> authMeter) {

        final Params p = new Params();
        try {
            System.out.println("setParam ===>" + nometer);
            String idAntrian = "";
            String collection = "";
            final BasicDBObject query = new BasicDBObject();
            final DBObject Projection = new BasicDBObject("_id", 0).append("LAST_ACTION", 0).append("ACTION", 0)
                    .append("ACTION_ON_SERVER", 0).append("ACTION_STATUS", 0).append("ID_ANTRIAN", 0);
            // System.out.println("authMeter");
            if (authMeter.get("idAntrian") != null && !authMeter.get("idAntrian").equals("")) {
                idAntrian = authMeter.get("idAntrian");
                query.put("ID_ANTRIAN", idAntrian);
                collection = "DATA_METER_ANTRIAN";
            } else {
                query.put("NO_METER", nometer);
                collection = "DATA_METER";
            }
            // System.out.println(collection + " : " + query.toString());
            // List<DBObject> cursor = MongoDB.getData("DATA_METER", query,
            // "",Projection);
            final List<DBObject> cursor = MongoDB.getData(collection, query, "", Projection);
            String noMeter = nometer;
            String typeMeter = "";
            String merkMeter = "";
            String authenticationLevel = "";
            String clientAddress = "";
            String logicalAddress = "";
            String physicalAddress = "";
            String ipAddress = "";
            String port = "";
            String passMeter = "";
            String security = "";
            String authenticationKey = "";
            String blockCipherKey = "";
            String systemTitle = "";
            Integer phasa = 0;
            String kdPusat = "";
            String kdPembangkit = "";
            String kdArea = "";
            String kdUnitBisnis = "";
            String jenis = "";
            String targetAddress = "";
            String interfaceType = "";
            String typeDevice = "";
            String noDevice = "";
            String typeObis = "";
            String noSeluler = "";
            String noSimcard = "";
            String addressSize = "2";
            DBObject csr = null;
            if (cursor.size() > 0) {
                csr = cursor.get(0);
                System.out.println("ADDRESS_SIZE : " + csr.get("ADDRESS_SIZE"));
                noMeter = csr.get("NO_METER").toString();
                typeMeter = csr.get("TIPE_METER").toString();
                merkMeter = csr.get("MERK_METER").toString();
                authenticationLevel = csr.get("AUTHENTICATION_LEVEL").toString();
                clientAddress = csr.get("CLIENT_ADDRESS").toString();
                logicalAddress = csr.get("LOGICAL_ADDRESS").toString();
                physicalAddress = csr.get("PHYSICAL_ADDRESS").toString();
                targetAddress = csr.get("TARGET_ADDRESS") == null ? "0" : csr.get("TARGET_ADDRESS").toString();
                ipAddress = csr.get("IP_ADDRESS").toString();
                port = csr.get("PORT").toString();
                passMeter = csr.get("PASSWORD_METER").toString();
                security = csr.get("SECURITY").toString();
                authenticationKey = csr.get("AUTHENTICATION_KEY").toString();
                blockCipherKey = csr.get("BLOCK_CIPHER_KEY").toString();
                systemTitle = csr.get("SYSTEM_TITLE").toString();
                phasa = Integer.parseInt(csr.get("PHASA").toString());
                kdPusat = csr.get("KD_PUSAT").toString();
                kdPembangkit = csr.get("KD_PEMBANGKIT").toString();
                kdArea = csr.get("KD_AREA").toString();
                kdUnitBisnis = csr.get("KD_UNIT_BISNIS").toString();
                jenis = csr.get("JENIS").toString();
                interfaceType = csr.get("INTERFACE_TYPE") == null ? "HDLC" : csr.get("INTERFACE_TYPE").toString();
                typeDevice = csr.get("TYPE_DEVICE") == null ? "" : csr.get("TYPE_DEVICE").toString();
                noDevice = csr.get("NO_DEVICE") == null ? "" : csr.get("NO_DEVICE").toString();
                typeObis = csr.get("TYPE_OBIS") == null ? "SPLN" : csr.get("TYPE_OBIS").toString();
                noSeluler = csr.get("NO_SELULER") == null ? "" : csr.get("NO_SELULER").toString();
                noSimcard = csr.get("NO_SIMCARD") == null ? "" : csr.get("NO_SIMCARD").toString();
                addressSize = csr.get("ADDRESS_SIZE") == null ? "" : csr.get("ADDRESS_SIZE").toString();
            }
            if (authMeter.size() > 0) {
                if (authMeter.get("authenticationLevel") != null && !authMeter.get("authenticationLevel").equals("")) {
                    authenticationLevel = authMeter.get("authenticationLevel");
                }

                if (authMeter.get("clientAddress") != null && !authMeter.get("clientAddress").equals("")) {
                    clientAddress = authMeter.get("clientAddress");
                }

                if (authMeter.get("logicalAddress") != null && !authMeter.get("logicalAddress").equals("")) {
                    logicalAddress = authMeter.get("logicalAddress");
                }

                if (authMeter.get("physicalAddress") != null && !authMeter.get("physicalAddress").equals("")) {
                    physicalAddress = authMeter.get("physicalAddress");
                }

                if (authMeter.get("ipAddress") != null && !authMeter.get("ipAddress").equals("")) {
                    ipAddress = authMeter.get("ipAddress");
                }

                if (authMeter.get("port") != null && !authMeter.get("port").equals("")) {
                    port = authMeter.get("port");
                }

                if (authMeter.get("passMeter") != null && !authMeter.get("passMeter").equals("")) {
                    passMeter = authMeter.get("passMeter");
                }

                if (authMeter.get("security") != null && !authMeter.get("security").equals("")) {
                    security = authMeter.get("security");
                }

                if (authMeter.get("authenticationKey") != null && !authMeter.get("authenticationKey").equals("")) {
                    authenticationKey = authMeter.get("authenticationKey");
                }

                if (authMeter.get("blockCipherKey") != null && !authMeter.get("blockCipherKey").equals("")) {
                    blockCipherKey = authMeter.get("blockCipherKey");
                }

                if (authMeter.get("systemTitle") != null && !authMeter.get("systemTitle").equals("")) {
                    systemTitle = authMeter.get("systemTitle");
                }

                if (authMeter.get("phasa") != null && !authMeter.get("phasa").equals("")) {
                    phasa = Integer.parseInt(authMeter.get("phasa"));
                }

                if (authMeter.get("merkMeter") != null && !authMeter.get("merkMeter").equals("")) {
                    merkMeter = authMeter.get("merkMeter");
                }

                if (authMeter.get("typeMeter") != null && !authMeter.get("typeMeter").equals("")) {
                    typeMeter = authMeter.get("typeMeter");
                }

                if (authMeter.get("targetAddress") != null && !authMeter.get("targetAddress").equals("")) {
                    targetAddress = authMeter.get("targetAddress");
                }

                if (authMeter.get("interfaceType") != null && !authMeter.get("interfaceType").equals("")) {
                    interfaceType = authMeter.get("interfaceType");
                }

                if (authMeter.get("noDevice") != null && !authMeter.get("noDevice").equals("")) {
                    noDevice = authMeter.get("noDevice");
                }

                if (authMeter.get("typeDevice") != null && !authMeter.get("typeDevice").equals("")) {
                    noDevice = authMeter.get("typeDevice");
                }

                if (authMeter.get("typeObis") != null && !authMeter.get("typeObis").equals("")) {
                    typeObis = authMeter.get("typeObis");
                }

                if (authMeter.get("noSeluler") != null && !authMeter.get("noSeluler").equals("")) {
                    noSeluler = authMeter.get("noSeluler");
                }

                if (authMeter.get("noSimcard") != null && !authMeter.get("noSimcard").equals("")) {
                    noSeluler = authMeter.get("noSimcard");
                }

                if (authMeter.get("kdPusat") != null && !authMeter.get("kdPusat").equals("")) {
                    kdPusat = authMeter.get("kdPusat");
                }

                if (authMeter.get("kdUnitBisnis") != null && !authMeter.get("kdUnitBisnis").equals("")) {
                    kdUnitBisnis = authMeter.get("kdUnitBisnis");
                }

                if (authMeter.get("kdPembangkit") != null && !authMeter.get("kdPembangkit").equals("")) {
                    kdPembangkit = authMeter.get("kdPembangkit");
                }

                if (authMeter.get("kdArea") != null && !authMeter.get("kdArea").equals("")) {
                    kdArea = authMeter.get("kdArea");
                }

                if (authMeter.get("jenis") != null && !authMeter.get("jenis").equals("")) {
                    jenis = authMeter.get("jenis");
                }

                if (authMeter.get("addressSize") != null && !authMeter.get("addressSize").equals("")) {
                    addressSize = authMeter.get("addressSize");
                }
            }
            p.setDbObject(csr);
            p.setAuthenticationLevel(authenticationLevel);
            p.setUseLogicalNameReferencing(true);

            p.setClientAddress(clientAddress.equals("null") ? 0 : Integer.parseInt(clientAddress));

            p.setLogicalAddress(logicalAddress.equals("null") ? 0 : Integer.parseInt(logicalAddress));

            p.setPhysicalAddress(physicalAddress.equals("null") ? 0 : Integer.parseInt(physicalAddress));

            p.setTargetAddress(targetAddress.equals("") ? 0 : Integer.parseInt(targetAddress));
            p.setPassword(passMeter);
            p.setTraceLevel("OFF");
            p.setHostName(ipAddress);
            if (port != null && port != "") {
                p.setPort(Integer.parseInt(port));
            }
            p.setSecurity(security);
            p.setAuthenticationKey(authenticationKey);
            p.setBlockCipherKey(blockCipherKey);
            p.setSystemTitle(systemTitle);
            p.setPhasa(phasa);
            p.setNoMeter(noMeter);
            p.setTypeMeter(typeMeter);
            p.setMerkMeter(merkMeter);
            p.setKdArea(kdArea);
            p.setKdPembangkit(kdPembangkit);
            p.setKdPusat(kdPusat);
            p.setKdUnitBisnis(kdUnitBisnis);
            p.setJenis(jenis);
            p.setInterfaceType(interfaceType);
            p.setNoDevice(noDevice);
            p.setTypeDevice(typeDevice);
            p.setTypeObis(typeObis);
            p.setNoSeluler(noSeluler);
            p.setNoSimcard(noSimcard);
            p.setAddressSize(Integer.parseInt(addressSize));
            final int ret = getParameters(client, p);
            if (ret != 0) {
                return null;
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
        return p;
    }

    static int getParameters(final GXDLMSSecureClient client, final Params p) {

        try {
            System.out.println("getParameters ===>" + p.getNoMeter());
            LOGGER.info("NoMeter : " + p.getNoMeter());
            LOGGER.info("Phasa : " + p.getPhasa());
            LOGGER.info("getUseLogicalNameReferencing : " + p.getUseLogicalNameReferencing());
            LOGGER.info("getAuthenticationLevel : " + p.getAuthenticationLevel());
            LOGGER.info("getClientAddress : " + p.getClientAddress());
            LOGGER.info("getTargetAddress : " + p.getTargetAddress());
            LOGGER.info("getPassword : " + p.getPassword());
            LOGGER.info("getHostName : " + p.getHostName());

            LOGGER.info("getPort : " + p.getPort());
            LOGGER.info("getLogicalAddress : " + p.getLogicalAddress());
            LOGGER.info("getPhysicalAddress : " + p.getPhysicalAddress());
            LOGGER.info("getAddressSize : " + p.getAddressSize());
            LOGGER.info("getAuthenticationKey : " + p.getAuthenticationKey());
            LOGGER.info("getSecurity : " + p.getSecurity());
            LOGGER.info("getBlockCipherKey : " + p.getBlockCipherKey());
            LOGGER.info("getSystemTitle : " + p.getSystemTitle());
            LOGGER.info("getInterface : " + p.getInterfaceType());
            LOGGER.info("IsDynamic : " + p.getIsDynamic());
            LOGGER.info("noDevice : " + p.getNoDevice());
            LOGGER.info("TypeObis : " + p.gettypeObis());

            // GXNet net = null;
            client.setUseLogicalNameReferencing(p.getUseLogicalNameReferencing());
            client.setAuthentication(Authentication.valueOfString(p.getAuthenticationLevel()));

            if (p.getClientAddress() != null && p.getClientAddress() != 0) {
                client.setClientAddress(p.getClientAddress());
            }

            // if (p.getHostName() != null && !p.getHostName().equals("")) {
            // // Host address.
            // if (settings.media == null) {
            // settings.media = new GXNet();
            // }
            // net = (GXNet) settings.media;
            // net.setHostName(p.getHostName());
            // }
            // if (p.getPort() != null && p.getPort() != 0) {
            // net.setPort(p.getPort());
            // }
            // settings.trace = p.getTraceLevel();
            // GXDLMSGateway gw = new GXDLMSGateway();
            // gw.setNetworkId((short)0);
            // gw.setPhysicalDeviceAddress(p.getPhysicalAddress().toString().getBytes());
            // settings.client.setGateway(null);
            // settings.iec = p.getUseIEC();
            // if (p.getUseIEC()) {
            // settings.client.setInterfaceType(InterfaceType.WRAPPER);
            // settings.client.setServerAddress(p.getPhysicalAddress());
            // } else {
            client.setInterfaceType(p.getInterfaceType());
            if (p.getLogicalAddress() > 0 || p.getClientAddress() > 0 || p.getTargetAddress() > 0) {
                if (p.getTargetAddress() > 0) {
                    client.setServerAddress(p.getTargetAddress());
                } else {
                    if (p.getInterfaceType().equals(InterfaceType.WRAPPER)) {
                        client.setServerAddress(p.getLogicalAddress());
                    } else {
                        client.setServerAddress(
                                client.getServerAddress(p.getLogicalAddress(), p.getPhysicalAddress(), p.getAddressSize()));
                    }
                }
            }
            // }

            // if (p.getSerialPort() != null && !p.getSerialPort().equals("")) {
            // settings.media = new GXSerial();
            // GXSerial serial = (GXSerial) settings.media;
            // serial.setPortName(p.getSerialPort());
            // }
            // Chipper Set
            if (p.getAuthenticationLevel().equals("HighGMac")) {
                client.getCiphering().setAuthenticationKey(p.getAuthenticationKey().getBytes("ASCII"));
                client.getCiphering().setSecurity(Security.AUTHENTICATION_ENCRYPTION);
                client.getCiphering().setBlockCipherKey(p.getBlockCipherKey().getBytes("ASCII"));
                client.getCiphering().setSystemTitle(p.getSystemTitle().getBytes("ASCII"));
                client.getCiphering().setInvocationCounter(1);
                // settings.client.getCiphering().setInvocationCounter(30105);
                // settings.client.getCiphering().setSecuritySuite(SecuritySuite.AES_GCM_128);
            } else {
                client.setPassword(p.getPassword().getBytes("ASCII"));
            }

            // if (p.getSelectedObject() != null && !p.getSelectedObject().equals("")) {
            // for (String o : p.getSelectedObject().split("[;,]")) {
            // String[] tmp = o.split("[:]");
            // if (tmp.length != 2) {
            // throw new IllegalArgumentException("Invalid Logical name or attribute
            // index.");
            // }
            // settings.readObjects
            // .add(new GXSimpleEntry<String, Integer>(tmp[0].trim(),
            // Integer.parseInt(tmp[1].trim())));
            // }
            // }
            // if (settings.media == null) {
            // // showHelp();
            // return 1;
            // }
            return 0;
        } catch (final Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public HashMap<String, String> MappingAuthMeter(final String AuthMeter) throws Exception {
        final HashMap<String, String> hmap = new HashMap<String, String>();
        if (AuthMeter != null && !AuthMeter.equals("")) {
            System.out.println("MappingAuthMeter ===>");
            final JSONParser parser = new JSONParser();
            System.out.println(new String(Base64.getDecoder().decode(AuthMeter)));
            final JSONObject json = (JSONObject) parser.parse(new String(Base64.getDecoder().decode(AuthMeter)));
            System.out.println(json);
            hmap.put("authenticationLevel", convertObject(json.get("authenticationLevel")));
            hmap.put("clientAddress", convertObject(json.get("clientAddress")));
            hmap.put("logicalAddress", convertObject(json.get("logicalAddress")));
            hmap.put("physicalAddress", convertObject(json.get("physicalAddress")));
            hmap.put("ipAddress", convertObject(json.get("ipAddress")));
            hmap.put("port", convertObject(json.get("port")));
            hmap.put("passMeter", convertObject(json.get("passMeter")));
            hmap.put("security", convertObject(json.get("security")));
            hmap.put("authenticationKey", convertObject(json.get("authenticationKey")));
            hmap.put("blockCipherKey", convertObject(json.get("blockCipherKey")));
            hmap.put("systemTitle", convertObject(json.get("systemTitle")));
            hmap.put("phasa", convertObject(json.get("phasa")));
            hmap.put("getResult", convertObject(json.get("getResult")));
            hmap.put("phasa", convertObject(json.get("phasa")));
            hmap.put("typeMeter", convertObject(json.get("typeMeter")));
            hmap.put("interfaceType", convertObject(json.get("interfaceType")));
            hmap.put("targetAddress", convertObject(json.get("targetAddress")));
            hmap.put("typeMeter", convertObject(json.get("typeMeter")));
            hmap.put("merkMeter", convertObject(json.get("merkMeter")));
            hmap.put("noDevice", convertObject(json.get("noDevice")));
            hmap.put("typeDevice", convertObject(json.get("typeDevice")));
            hmap.put("typeObis", convertObject(json.get("typeObis")));
            hmap.put("noSeluler", convertObject(json.get("noSeluler")));
            hmap.put("noSimcard", convertObject(json.get("noSimcard")));
            hmap.put("kdPusat", convertObject(json.get("kdPusat")));
            hmap.put("kdUnitBisnis", convertObject(json.get("kdUnitBisnis")));
            hmap.put("kdPembangkit", convertObject(json.get("kdPembangkit")));
            hmap.put("kdArea", convertObject(json.get("kdArea")));
            hmap.put("jenis", convertObject(json.get("jenis")));
            hmap.put("idAntrian", convertObject(json.get("idAntrian")));
            hmap.put("addressSize", convertObject(json.get("addressSize")));
        }
        return hmap;
    }

    private String convertObject(final Object obj) {
        if (obj != null) {
            return (String) obj;
        } else {
            return "";
        }
    }
}
