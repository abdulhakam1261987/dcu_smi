package id.co.tabs.hes.dcu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import gurux.common.GXCommon;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXStructure;
import gurux.dlms.enums.Command;
import gurux.net.GXNet;
import id.co.tabs.hes.dlms.GXFCS16;

public class DCUManager {
    private static final Logger LOGGER = Logger.getLogger(DCUManager.class.getName());

    private static final DCUManager instance;
    private static final byte[] lnPushSetupOnConnectivity = { 0x00, 0x00, 0x19, 0x9, 0x00, (byte) 0xFF };
    private static final byte[] lnPushSetupOnConnectivitySMI = { 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x16, (byte) 0x0A };
    private static final byte[] lnDCUSerialNumber = { 0x00, 0x00, 0x60, 0x01, 0x00, (byte) 0xFF };


    private List<DCU> dcus = new ArrayList<DCU>();
    private List<IDCUManagerListener> listeners = new ArrayList<IDCUManagerListener>();
    private ReadWriteLock rwLock = new ReentrantReadWriteLock();

    private DCUManager() {
    }

    static {
        try {
            instance = new DCUManager();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static DCUManager getInstance() {
        return instance;
    }

    public final void addListener(final IDCUManagerListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public final void removeListener(final IDCUManagerListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }

    public Boolean isMeterRegistrationMessage(GXReplyData data) {
        if (data.getCommand() == Command.DATA_NOTIFICATION) {
            if (data.getValue() instanceof GXStructure) {
                GXStructure info = (GXStructure) data.getValue();
                if ((info.size() > 0) && Arrays.equals((byte[]) info.get(0), lnPushSetupOnConnectivity)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Boolean isMeterRegistrationSMIMessage(byte[] data) {
        LOGGER.log(Level.INFO, "Meter Registration SMI Message");
                byte[] bytes = data;
                if (data.length > 0 && bytes[8]==(byte) 0x0A) {
                    byte[] crcByte = getCRCSMI(data, 2);
                    int idx_dt = 0;
                    byte[] dtByte = new byte[9];
                    for(int i = 0 ; i<9; i++){
                            dtByte[idx_dt] = bytes[i];
                            idx_dt++;
                    }

                    byte[] crcByte2 = genCRCSMI(data, data.length-2-9, 10, data.length-2);
                    if (Arrays.equals(dtByte, lnPushSetupOnConnectivitySMI)&&Arrays.equals(crcByte2, crcByte)) {
                        return true;
                    }
                }
        return false;
    }

    public final byte[] genCRCSMI(final byte[] data, final int lengthInfo, final int idxAwal, final int idxAkhir) {
        byte[] infoByte = new byte[lengthInfo];
        int idx_info = 0;
        for(int i = idxAwal-1 ; i<idxAkhir; i++){
            // if(i>idxAwal && i<idxAkhir){
                infoByte[idx_info] = data[i];
                idx_info++;
            // }
        }
        int crc = GXFCS16.countFCS16(infoByte, 0, infoByte.length);
        byte[] crcByte2 = setUInt16(0,crc);
        return crcByte2;
    }

    public final byte[] getCRCSMI(final byte[] data,final int lengthCRC) {
        byte[] crcByte = new byte[lengthCRC];
        int idx_crc = 0;
        for(int i = data.length-lengthCRC ; i<data.length; i++){
                crcByte[idx_crc] = data[i];
                idx_crc++;
        }
        return crcByte;
    }

    public final byte[] setUInt16(final int index, final int item) {
        byte[] data = new byte[2];
        data[index] = (byte) ((item >> 8) & 0xFF);
        data[index + 1] = (byte) (item & 0xFF);
        return data;
    }

    final void notifyDCUConnected(final DCU dcu) {
        synchronized(listeners) {
            for (IDCUManagerListener it : listeners) {
                it.onDCUConnected(dcu);
            }
        }
    }

    final void notifyDCUDisconnected(final DCU dcu) {
        synchronized(listeners) {
            for (IDCUManagerListener it : listeners) {
                it.onDCUDisconnected(dcu);
            }
        }
    }

    final void notifyDCUHeartbeat(final DCU dcu) {
        synchronized(listeners) {
            for (IDCUManagerListener it : listeners) {
                it.onDCUHeartbeat(dcu);
            }
        }
    }

    public DCU register(GXNet media, String senderInfo, GXReplyData data) {
        if (data.getValue() instanceof GXStructure) {
            GXStructure info = (GXStructure) data.getValue();
            if ((info.size() > 0) && Arrays.equals((byte[]) info.get(0), lnPushSetupOnConnectivity)
                    && Arrays.equals((byte[]) info.get(1), lnDCUSerialNumber)) {
                String serialNumber = new String((byte[]) info.get(2));
                DCU dcu = getDCUBySN(serialNumber);
                if (dcu == null) {
                    dcu = new DCU(media, senderInfo, serialNumber, (byte[]) info.get(2), "WASION");
                    rwLock.writeLock().lock();
                    try {
                        dcus.add(dcu);
                    } finally {
                        rwLock.writeLock().unlock();
                    }
                } else {
                    dcu.setIPAddress(senderInfo);
                }
                dcu.setIsOnline(true);
                LOGGER.log(Level.INFO,
                        "Registered DCU with serial number " + dcu.getSerialNumber() + " at " + senderInfo);
                notifyDCUConnected(dcu);
                return dcu;
            }
        }

        LOGGER.log(Level.WARNING, "Invalid DCU registration message.");
        return null;
    }

    public DCU registerSMI(GXNet media, String senderInfo, byte[] data) {
        // if (data.getValue() instanceof GXStructure) {
            // GXStructure info = (GXStructure) data.getValue();
            if ((data.length > 0)) {
                int lng = data[10];
                byte[] snByte = new byte[lng];
                int idx_sn = 0;
                for(int i = 0 ; i<lng; i++){
                    snByte[idx_sn] = data[i+11];
                    idx_sn++;
                }
                String serialNumber = new String(snByte);
                DCU dcu = getDCUBySN(serialNumber);
                if (dcu == null) {
                    dcu = new DCU(media, senderInfo, serialNumber, snByte, "SMI");
                    rwLock.writeLock().lock();
                    try {
                        dcus.add(dcu);
                    } finally {
                        rwLock.writeLock().unlock();
                    }
                } else {
                    dcu.setIPAddress(senderInfo);
                }
                dcu.setIsOnline(true);
                LOGGER.log(Level.INFO,
                        "Registered DCU with serial number " + dcu.getSerialNumber() + " at " + senderInfo);
                notifyDCUConnected(dcu);
                return dcu;
            }
        // }

        LOGGER.log(Level.WARNING, "Invalid DCU registration message.");
        return null;
    }

    public Boolean processHeartbeat(DCU dcu, final byte[] packet) {
        if (dcu != null) {
            Boolean processed = dcu.processHeartbeat(packet);
            if (processed)
                notifyDCUHeartbeat(dcu);
            return processed;
        }
        return false;
    }

    public Boolean processHeartbeatSMI(DCU dcu, final byte[] packet) {
        // System.out.println("processHeartbeatSMI...@"+dcu.getSerialNumber());

        if (dcu != null) {
            Boolean processed = dcu.processHeartbeatSMI(packet);
            if (processed)
                notifyDCUHeartbeat(dcu);
            return processed;
        }else{
            System.out.println("processHeartbeatSMI...@"+dcu.getSerialNumber()+" DCU NULL");
        }
        return false;
    }

    public void connected(String ipAddress) {
        LOGGER.log(Level.INFO, "Incoming DCU connection from " + ipAddress);
    }

    public void disconnect(String ipAddress) {
        DCU dcu = getDCUByIP(ipAddress);
        if (dcu != null) {
            LOGGER.log(Level.INFO, "DCU " + dcu.getSerialNumber() + " at" + ipAddress + " disconnected.");
            dcu.setIsOnline(false);
            notifyDCUDisconnected(dcu);
        }
    }

    public DCU getDCUByIP(String target) {
        rwLock.readLock().lock();
        try {
            for (DCU d : dcus) {
                if (d.getIPAddress().equals(target)) {
                    return d;
                }
            }
        } finally {
            rwLock.readLock().unlock();
        }
        return null;
    }

    public DCU getDCUBySN(String target) {
        rwLock.readLock().lock();
        try {
            for (DCU d : dcus) {
                if (d.getSerialNumber().equals(target)) {
                    return d;
                }
            }
        } finally {
            rwLock.readLock().unlock();
        }
        return null;
    }
}
