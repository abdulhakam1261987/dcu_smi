package id.co.tabs.hes;

public class DataLogicalName {
	String ln;
	String logicalName;
	String val;
	String classId;
	/**
	 * @return the ln
	 */
	public String getLn() {
		return ln;
	}
	/**
	 * @param ln the ln to set
	 */
	public void setLn(String ln) {
		this.ln = ln;
	}
	/**
	 * @return the logicalName
	 */
	public String getLogicalName() {
		return logicalName;
	}
	/**
	 * @param logicalName the logicalName to set
	 */
	public void setLogicalName(String logicalName) {
		this.logicalName = logicalName;
	}
	/**
	 * @return the val
	 */
	public String getVal() {
		return val;
	}
	/**
	 * @param val the val to set
	 */
	public void setVal(String val) {
		this.val = val;
	}
	/**
	 * @return the classId
	 */
	public String getClassId() {
		return classId;
	}
	/**
	 * @param classId the classId to set
	 */
	public void setClassId(String classId) {
		this.classId = classId;
	}



}
